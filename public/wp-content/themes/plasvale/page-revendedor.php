<?php
    wp_enqueue_style('css_revendedores', get_stylesheet_directory_uri().'/src/css/revededores.min.css', array(), null, false);
    
    get_header();
?>

<div class="container-revededores" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <div class="container-pequeno">
        <div class="container-inicial">

            <div class="titulo"><?= get_field('titulo'); ?></div>
            <div class="descricao"><?= get_field('descricao'); ?></div>

            <div class="container-pontos">
                <div class="texto"><?= get_field('sessao_topicos')['texto']; ?></div>
                <?php
                
                    $todos_topicos = get_field('sessao_topicos')['topicos'];

                    for( $i = 0; $i < count( $todos_topicos ); $i ++ ){

                        $topico = $todos_topicos[$i];
                
                ?>
                <div class="ponto">
                    <img src="<?= $topico['icone']; ?>" alt="Icone <?= $topico['titulo']; ?>">
                    <div class="informacoes">
                        <h2><?= $topico['titulo']; ?></h2>
                        <div class="texto"><?= $topico['texto']; ?></div>
                    </div>
                </div>
                <?php
                
                    }
                
                ?>
            </div>

        </div>
        <div class="container-formulario">
            <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                <?php
                
                    $campos = get_field('formulario_de_contato')['campos'];

                    for( $i = 0; $i < count( $campos ); $i ++ ){

                        $campo = $campos[$i];
                
                ?>
                <label for="<?= $campo['nome']; ?>"><?= $campo['nome']; ?></label>
                <input type="text" name="<?= $campo['nome']; ?>" required hc-mail-message placeholder="<?= $campo['placeholder']; ?>">
                <?php
                
                    }
                
                ?>
                <div class="termos-privacidade">
                    <input required type="checkbox" name="checkbox"> 
                    <p><?= get_field('formulario_de_contato')['texto_politica_de_privacidade']; ?></p>
                </div>
                <div class="botao">
                    <button type="submit" class="botao"><?= get_field('formulario_de_contato')['texto_botao']; ?></button>
                </div>                
                <a href="<?= get_field('formulario_de_contato')['link_botao_ultimo']; ?>" target="_blank" rel="noopener noreferrer"><?= get_field('formulario_de_contato')['texto_botao_ultimo']; ?></a>
                <span data-hc-feedback></span>
            </form>
        </div>
    </div>    
</div>

<script>

    jQuery(document).ready(function($){

        $('.telefone').mask('(00) 00000-0000');

    });

</script>

<?php
    get_footer();
?> 