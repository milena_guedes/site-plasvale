<?php
//******* LINKS PARA SCRIPTS CSS E JS */

/******* Scripts padrões *******/
include __DIR__."/incs/links_base.php"; 

/******* Links de plugins *******/
include __DIR__."/incs/links_plugins.php"; 

//****** CUSTOM */

/******* Custom config *******/
include __DIR__."/incs/custom_config.php";

/******* Custom image size *******/
include __DIR__."/incs/custom_image_sizes.php";

/******* Custom URL *******/
include __DIR__."/incs/custom_url.php";

/******* AJAX Functions *******/
include __DIR__."/incs/ajax.php";


//****** INFORMAÇÕES GLOBAIS

//include __DIR__."/incs/custom_info.php";

//******* ACF OPTIONS */

/******* Configurações para o acf options ********/
include __DIR__."/incs/acf_options.php";

/******* Custom post types *******/
include __DIR__."/incs/custom_post_type.php";

/******* Custom taxonomy *******/
include __DIR__."/incs/custom_tax.php";


function be_ajax_load_more() {

    $postsCarregado = 0;

    $args = array(
        'numberposts'      => 16,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'posts_per_page' => 16,
        'paged' => $_POST['page'],
        'post_type'  => 'empreendimentos',
        'tax_query' => $_POST['catquery']
    );

    $postsCarregado = get_posts($args);

    $containerResponse = array( );
    $countPosts = count($postsCarregado);
    if($countPosts == 0){
        echo 'false';
        die();
    }else{
        for($i = 0; $i < $countPosts; $i++){

            $titulo = $postsCarregado[$i]->post_title;  

            $logo = get_field('sobre_empreendimento', $postsCarregado[$i]->ID)['logo_destaque_card'];
            
            $descricao = get_field('bloco_banner_inicial', $postsCarregado[$i]->ID)['texto_inicial'];

            if (strlen($descricao) > 100){
                $descricao = substr($descricao, 0, 97)."..."; 
            }else{
                $descricao = $descricao;
            }

            $link = get_home_url() . '/empreendimento/' .  $postsCarregado[$i]->post_name; 

            $imagem = get_field( 'sobre_empreendimento', $postsCarregado[$i]->ID )['imagem_em_destaque_card'];

            $informacoes = get_field('sobre_empreendimento', $postsCarregado[$i]->ID )['informacoes_sobre_a_obra'];

            $icone1 = $informacoes[0]['icone'];
            $descricao1 = $informacoes[0]['descricao'];

            $icone2 = $informacoes[1]['icone'];
            $descricao2 = $informacoes[1]['descricao'];

            $icone3 = $informacoes[2]['icone'];
            $descricao3 = $informacoes[2]['descricao']; 

            array_push( $containerResponse, array(
                                                "titulo" => $titulo, "logo" => $logo, "descricao" => $descricao, "link" => $link, 
                                                "imagem" => $imagem, "icone1" => $icone1, "icone2" => $icone2, "icone3" => $icone3,
                                                "descricao1" => $descricao1, "descricao2" => $descricao2, "descricao3" => $descricao3
            ));
        }

        $postsJson = json_decode($postsCarregado, true);

        echo json_encode($containerResponse);
        die();
    }
}

function transformToMediumText($title){
    if (strlen($title) < 130) {
        return $title;
    } else {

      $new = wordwrap($title, 130);

      $new = explode("\n", $new);
    
      $new = $new[0] . '...';
    
      return $new;
    }
}

function transformToSmallText($title){
    if (strlen($title) < 80) {
        return $title;
    } else {

      $new = wordwrap($title, 80);

      $new = explode("\n", $new);
    
      $new = $new[0] . '...';
    
      return $new;
    }
}

function contadorLancamentos($categoria){

    $tax_query = array(
        array(
            'taxonomy' => 'categoria',
            'field' => 'slug',
            'terms' => array( $categoria ),
            'hide_empty' => false,
        ),
    );

    $meta_query = array(
        array(
            'key' => 'esse_produto_e_um_lancamento',
            'value' => true,
            'compare' => '='
        ),
    );
    
    $argumentos = array(
        'post_type' => 'produtos',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'tax_query' => array( $tax_query ),
        'meta_query'=> array( $meta_query )           
    );

    $produtos = get_posts($argumentos);

    $contados_lancamentos = count($produtos);

    return $contados_lancamentos;

}

//Detect post views count and store it as a custom field for each post
function wpb_set_post_views($postID) {

    $count_key = 'wpb_post_views_count';

    $count = get_post_meta($postID, $count_key, true);

    if($count==''){

        $count = 0;

        delete_post_meta($postID, $count_key);

        add_post_meta($postID, $count_key, '0');

    }else{
        
        $count++;

        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

add_action( 'wp_ajax_be_ajax_load_more', 'be_ajax_load_more' );
add_action( 'wp_ajax_nopriv_be_ajax_load_more', 'be_ajax_load_more' );

