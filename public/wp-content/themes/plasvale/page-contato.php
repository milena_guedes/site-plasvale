<?php
    wp_enqueue_style('css_contato', get_stylesheet_directory_uri().'/src/css/contato.min.css', array(), null, false);

    get_header();
?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <h1><?= get_field('titulo'); ?></h1>
    <div class="container-texto">
        <?= get_field('texto'); ?>
    </div>
</div>

<div class="container-informacoes-contato">
    <div class="container-medio">
        <div class="container-formulario">
            <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                <div class="linha">
                    <input type="text" name="nome" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['texto_input_nome']; ?>">
                    <div class="vazia"></div>
                    <input type="text" name="sobrenome" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['texto_input_sobrenome']; ?>">
                </div>
                <div class="linha">
                    <input type="text" name="email" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['texto_input_email']; ?>">
                    <div class="vazia"></div>
                    <input type="text" name="cep" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['texto_input_cep']; ?>">
                </div>
                <div class="linha">
                    <input type="text" name="cidade" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['texto_input_cidade']; ?>">
                    <div class="vazia"></div>
                    <input type="text" name="estado" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['texto_input_estado']; ?>">
                </div>
                <div class="container-input-upload" id="container-input-upload">
                    <div class="linha">
                        <p><?= get_field('formulario_de_contato')['campo_anexar_arquivo']['texto_input']; ?></p>
                        <button type="button" class="botao abrir-upload">
                            <?= get_field('formulario_de_contato')['campo_anexar_arquivo']['texto_botao']; ?>
                        </button>
                        <input type="file" name="Arquivo" id="arquivo">
                    </div>                    
                    <span id="arquivo-anexado" class="fonte-anexo"></span>
                </div>
                <select name="departamento" id="select-departamento">
                    <option value="primeira-opcao"><?= get_field('formulario_de_contato')['campo_departamentos']['texto_input_departamentos']; ?></option>
                    <?php
                    
                        $todas_opcoes = get_field('formulario_de_contato')['campo_departamentos']['opcoes'];

                        for( $i = 0; $i < count( $todas_opcoes ); $i ++ ){

                            $opcao = $todas_opcoes[$i]['opcao'];    

                    ?>
                    <option value="<?= $opcao; ?>"><?= $opcao; ?></option>
                    <?php
                    
                        }
                    
                    ?>
                </select>
                <input type="text" name="assunto" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['texto_input_assunto_mensagem']; ?>">
                <textarea name="mensagem" required hc-mail-message placeholder="Digite aqui a sua mensagem…"></textarea>
                <button type="submit" class="botao"><?= get_field('formulario_de_contato')['texto_botao_enviar']; ?></button>
                <span data-hc-feedback></span>
            </form>
        </div>
        <div class="container-contatos">
            <div class="container-informacoes">
                <div class="container-informacoes-iniciais">
                    <?php

                        $informacoes = get_field('informacoes_de_contato_iniciais'); 
                            
                        for( $i = 0; $i < count( $informacoes ); $i ++ ){

                            $informacao = $informacoes[$i];

                    ?>
                    <div class="informacao">
                        <?= $informacao['informacao']; ?>
                    </div>
                    <?php

                        }
                        
                    ?>
                    <div class="container-redes">
                        <?php

                            $redes = get_field('todas_as_redes', 'geral'); 
                                                    
                            for( $i = 0; $i < count( $redes ); $i ++ ){

                                $rede = $redes[$i];

                        ?>
                        <a href="<?= $rede['url']; ?>"><img src="<?= $rede['icone_preta']; ?>" alt="Ícone da Rede <?= $rede['nome_da_rede']; ?>"></a>
                        <?php

                            }
                                
                        ?>
                    </div>
                </div>
            </div>
            <div class="container-outras-informacoes">
                <?php

                    $outras_informacoes = get_field('outras_informacoes_contato'); 
                                            
                    for( $i = 0; $i < count( $outras_informacoes ); $i ++ ){

                        $outra_informacao = $outras_informacoes[$i];

                ?>
                <div class="container-informacao">
                    <h1><?= $outra_informacao['nome']; ?></h1>
                    <?php

                        $dados_informacao = $outra_informacao['informacoes']; 
                                                
                        for( $j = 0; $j < count( $dados_informacao ); $j ++ ){

                            $dado = $dados_informacao[$j];

                    ?>
                    <div class="linha">
                        <img src="<?= $dado['icone']; ?>" alt="Icone de <?= $outra_informacao['nome']; ?>">
                        <p><?= $dado['informacao']; ?></p>
                    </div>
                    <?php
                
                        }
                    
                    ?>
                </div>
                <?php
                
                    }
                
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container-map-pai anime anime-fade" style="background-image: url('<?= get_field('sessao_mapa')['imagem_mapa']; ?>')">
    <div class="container-conteudo">
        <div class="card-informacoes">

            <?php $informacao = get_field('sessao_mapa')['informacoes_card_mapa']; ?>

            <div class="informacao">
                <img src="<?= $informacao['endereco']['icone']; ?>" alt="Ícone endereço">
                <p><?= $informacao['endereco']['texto']; ?></p>
            </div>

            <div class="informacao">
                <img src="<?= $informacao['cidade_estado']['icone']; ?>" alt="Ícone cidade/estado">
                <p><?= $informacao['cidade_estado']['texto']; ?></p>
            </div>

            <div class="informacao">
                <img src="<?= $informacao['telefone']['icone']; ?>" alt="Ícone telefone">
                <p><?= $informacao['telefone']['texto']; ?></p>
            </div>

            <div class="informacao">
                <img src="<?= $informacao['email']['icone']; ?>" alt="Ícone email">
                <p><?= $informacao['email']['texto']; ?></p>
            </div>
            <img src="<?=get_stylesheet_directory_uri()?>/img/arrow-down-filled-triangle.svg" alt="Pointer" class="pointer">
        </div>
        <img src="<?=get_stylesheet_directory_uri()?>/img/pin-plasvale.png" alt="Logo Plasvale" class="logo">
    </div>
</div>

<div class="container-faq">
    <div class="container-padrao">
        <div class="container-inicial">
            <img src="<?= get_field('sessao_faq')['imagem']; ?>" alt="Ícone sessão FAQ">
            <h2><?= get_field('sessao_faq')['titulo']; ?></h2>
            <div class="texto">
                <?= get_field('sessao_faq')['descricao']; ?>
            </div>
        </div>
        <div class="container-perguntas">
            <?php
            
                $perguntas = get_field('sessao_faq')['perguntas'];

                $total_perguntas = count( $perguntas );

                for( $i = 0; $i < $total_perguntas; $i ++ ){

                    $pergunta = $perguntas[$i];

                    

                    if( $i == $total_perguntas - 1 ){

                        $ultimo = 'border-width: 2px 2px 0px 2px';

                        $ultimo_texto = 'border-width: 0 2px 2px 2px;';

                    }else{
                        $ultimo = '';

                        $ultimo_texto = '';
                    }
            
            ?>
            <div class="opcao">
                <div class="container-selected" onclick="abrirDropDown('<?= $i + 1; ?>')" style="<?= $ultimo; ?>">
                    <h3 id="texto-selected"><?= $pergunta['pergunta']; ?></h3>
                    <div class="container-seta">
                        <img id="seta-<?= $i + 1; ?>" src="<?php echo get_template_directory_uri()?>/img/chevron-top-solid.png">
                    </div>
                </div>
                <div class="container-options" id="container-options-<?= $i + 1; ?>" style="<?= $ultimo_texto; ?>">
                    <div class="container-resposta">
                        <?= $pergunta['resposta']; ?>
                    </div>                    
                </div>
            </div>
            <?php
            
                }
            
            ?>
        </div>
    </div>
</div>

<script>

    jQuery(document).ready(function($){

        var departamento = $("#select-departamento option:selected").val();
        
        $('#select-departamento').on('change', function () {

            var departamento = $("#select-departamento option:selected").val();

            if( departamento == 'Recursos Humanos' ){                

                $('#container-input-upload').addClass('ativo');

            }else{

                $('#container-input-upload').removeClass('ativo');

            }
            
        });

    });
   
</script>
    

<?php get_footer();?>