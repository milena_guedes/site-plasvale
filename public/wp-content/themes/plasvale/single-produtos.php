<?php
    wp_enqueue_style('css_single_produtos', get_stylesheet_directory_uri().'/src/css/single-produtos.min.css', array(), null, false);
    
    get_header();

    $idioma = pll_the_languages( array( 'raw' => 1 ) );

	if( $idioma['pt']['current_lang'] == true ){ 

		$titulo_pagina_produtos = 'Nossos produtos';

        $botao_voltar = 'Voltar';

        $id_pagina_produto = 683;

        $titulo_cores = 'Cores';
	}

	if( $idioma['en']['current_lang'] == true ){ 

		$titulo_pagina_produtos = 'Our Products';

        $botao_voltar = 'Back';

        $id_pagina_produto = 685;

        $titulo_cores = 'Colors';

	}

    if( $idioma['es']['current_lang'] == true ){ 

		$titulo_pagina_produtos = 'Nuestros Productos';

        $botao_voltar = 'Regresar';

        $id_pagina_produto = 687;

        $titulo_cores = 'Colores';

	}

    $term_list = wp_get_post_terms( $post->ID, 'categoria', array( 'orderby' => 'parent', 'order' => 'ASC' ) );

    $nome_categoria_post = $term_list[0]->name;

?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('banner_inicial'); ?>'); ">
    <h1><?= $nome_categoria_post; ?></h1>
    <div class="container-texto">
        <?= 'Home ' . '> ' . $titulo_pagina_produtos . ' > ' . $nome_categoria_post . ' > ' . get_the_title($post); ?>
    </div>
</div>

<div class="container-sobre-produto">
    <div class="container-pequeno">
        <div class="container-imagens">
            <h1><?= get_the_title(); ?></h1>
            <h2><?= get_field('ref'); ?></h2>
            <div class="container-carrossel-imagens">
                <div class="container-imagem-destaque">
                    <?php
                    
                        $imagens = get_field('imagens_do_produto');

                        for( $i = 0; $i < count( $imagens ); $i ++  ){

                            $imagem = $imagens[$i];          
                            
                            $i == 0 ? $ativo = 'ativo' : $ativo = '';
                    ?>
                    <img src="<?= $imagem['imagem']; ?>" id="<?= $i + 1; ?>" onClick="document.getElementById('modal-wrapper').style.display='block'" class="imagem-destaque-<?= $i + 1; ?> imagem-detaque <?= $ativo; ?>" alt="<?= 'Imagem do produto: ' . get_the_title() . ' ' . $imagem['nome_da_cor']; ?>">
                    <?php
                    
                        }

                    ?>
                </div>
                <div class="container-miniaturas">
                    <?php
                        
                        $imagens = get_field('imagens_do_produto');

                        for( $i = 0; $i < count( $imagens ); $i ++  ){

                            $imagem = $imagens[$i];   
                            
                            $i == 0 ? $desativo = 'desativo' : $desativo = '';
                    ?>
                    <img src="<?= $imagem['imagem']; ?>" class="imagem-miniatura <?= $desativo; ?> imagem-produto-<?= $i + 1; ?>" alt="<?= 'Imagem do produto: ' . get_the_title() . ' ' . $imagem['nome_da_cor']; ?>">
                    <?php
                    
                        }

                    ?>
                </div>
            </div>
            <div class="mensagem-pai">
                <div class="mensagem">
                <img src="<?= get_stylesheet_directory_uri() ?>/img/360-degrees.png" alt="ícone 360deg">
                    <p><?= get_field('textos_380_viewer')['titulo_card_pequeno']; ?></p>
                </div>
            </div>
        </div>
        <div class="container-especificacoes">

            <a href="<?= get_permalink($id_pagina_produto); ?>" class="botao-voltar">
                <img src="<?= get_stylesheet_directory_uri() ?>/img/right-arrow.svg" alt="Seta Voltar">
                <p><?= $botao_voltar; ?></p>
            </a>

            <div class="descricao">
                <?= get_field('descricao'); ?>
            </div>
            <div class="todas-caracteristicas-pai">
                <div class="outras-caracteristicas">
                    <?php
                    
                        $todas_caracteristicas = get_field('caracteriticas')['outras_caracteristicas'];

                        for( $i = 0; $i < count( $todas_caracteristicas ); $i ++ ){

                            $caracteristica = $todas_caracteristicas[$i];
                    
                    ?>
                    <div class="caracteristica">
                        <img src="<?= $caracteristica['icone']; ?>" alt="ícone caracteristica <?= $caracteristica['texto']; ?>">
                        <p><?= $caracteristica['texto']; ?></p>
                    </div>
                    <?php

                        }
                    
                    ?>
                </div>
                
                <div class="lista-caracteristicas">
                    <?= get_field('caracteriticas')['lista_de_caracteristicas']; ?>
                </div>
                <div class="cores-disponiveis">
                    <h1><?= $titulo_cores; ?></h1>
                    <div class="todas-cores">
                        <?php
                        
                            $todas_cores = get_field('imagens_do_produto');

                            for( $i = 0; $i < count(  $todas_cores ); $i ++ ){

                                $cor = $todas_cores[$i];
                        
                        ?>
                        <div class="cor" style="background-color: <?= $cor['cor']; ?>" onclick="mudarImagem(<?= $i + 1; ?>);">
                            <p><?= $cor['nome_da_cor']; ?></p>
                        </div>

                        <?php 
                        
                            }
                        ?>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-cards">
    <div class="container-formulario">
        <h1><?= get_field('formulario_cotacao')['titulo']; ?></h1>
        <p><?= get_field('formulario_cotacao')['descricao']; ?></p>
        <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
            <input type="hidden" name="<?= 'Produto: ' . get_the_title(); ?>">
            <div class="container-campos">
                <?php
                    
                    $campos = get_field('formulario_cotacao')['campos'];

                    //var_dump($campos);

                    for( $i = 0; $i < count( $campos ); $i ++ ){

                        $campo = $campos[$i];
                    
                ?>
                <input type="text" name="<?= $campo['nome']; ?>" required hc-mail-message placeholder="<?= $campo['input']; ?>">
                <?php
                    
                    }
                    
                ?>
            </div>            
            <div class="botao">
                <button type="submit" class="botao"><?= get_field('formulario_cotacao')['texto_botao']; ?></button>
            </div>                
            <span data-hc-feedback></span>
        </form>
    </div>
    <div class="container-encontrar-produto">
        <h1><?= get_field('encontrar_produto')['titulo']; ?></h1>
        <div class="descricao">
            <?= get_field('encontrar_produto')['descricao']; ?>
        </div>
        <a href="<?= get_field('encontrar_produto')['url_botao']; ?>"><p><?= get_field('encontrar_produto')['texto_botao']; ?></p></a>
    </div>
</div>

<div class="container-produtos-relacionados">
    <div class="container-pequeno">
        <h1><?= get_field('titulo_produtos_relacionados'); ?></h1>
        <div class="carrossel-produtos-pai">
        
            <div class="seta seta-anterior-lancamentos"> 
                <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-left-solid.png">  
            </div>
          
            <div class="carrossel-produtos-filho">
                <?php

                    $produtos = get_field('produtos_relacionados');

                    for( $i = 0; $i < count( $produtos ); $i ++ ){

                        $produto = $produtos[$i]['produto'];

                        $term_list = wp_get_post_terms( $produto->ID, 'categoria', array( 'orderby' => 'parent', 'order' => 'ASC' ) );

                        $id_categoria = $term_list[0]->term_id;

                        if( !empty( $id_categoria ) ){

                            $cor = get_field('cor_de_fundo', 'categoria' . '_' . $id_categoria);

                        }else{

                            $cor = '#abd6cb';
                        }

                    ?>

                    <a href="<?= get_permalink( $produto->ID ); ?>" class="card-produto" style="border-bottom: 0px solid <?= $cor; ?>">                       
                        <img src="<?= get_field('imagens_do_produto', $produto->ID)[0]['imagem']; ?>" alt="<?= 'Imagem do produto ' . $produto->post_title; ?>">
                        <h2><?= $produto->post_title; ?></h2>
                        <p><?= get_field('ref', $produto->ID); ?></p>
                    </a>

                    <?php

                    }

                ?>
            
            </div>
            
            <div class="seta seta-proxima-lancamentos">
                <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-right-solid.png">  
            </div>
        
        </div>
    </div>            
</div>

<div id="modal-wrapper" class="modal">
    <div class="center">
        <span onClick="document.getElementById('modal-wrapper').style.display='none'" class="close">&times;</span>
        <p><?= get_field('textos_380_viewer')['titulo_card_maior']; ?></p>
        <div class="rotation">
            <?php
                
                $imagens360 = get_field('imagens_360_viewer');

                for( $i = 0; $i < count( $imagens360 ); $i ++ ){

                    $imagem = $imagens360[$i]['imagem'];
                
            ?>

            <img src="<?= $imagem; ?>">
            <?php
            
                }
            
            ?>

        </div>
    </div>
</div>

<script>

    function mudarImagem(imagemDestaque){

        //ao clicar na cor

        //remover imagem destacada ativa
        var imagemDestaqueAtiva = $('.container-sobre-produto .container-pequeno .container-imagens .container-carrossel-imagens .container-imagem-destaque .ativo').attr('id');
        
        jQuery('.imagem-destaque-' + imagemDestaqueAtiva).removeClass('ativo');

        //remover miniatura da cor clicada 
        jQuery('.imagem-produto-' + imagemDestaque).addClass('desativo');

        //ativar imagem destacada relativa a cor 
        jQuery('.imagem-destaque-' + imagemDestaque).addClass('ativo');

        if( imagemDestaqueAtiva != imagemDestaque ){

            //ativar miniatura da cor que estava ativa
            jQuery('.imagem-produto-' + imagemDestaqueAtiva).removeClass('desativo');

            jQuery('.imagem-produto-' + imagemDestaqueAtiva).addClass('ativo');
        }

    }

    jQuery(document).ready(function ($) {

        $('.carrossel-produtos-filho').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: false,
            prevArrow: $('.seta-anterior-lancamentos'),
            nextArrow: $('.seta-proxima-lancamentos'),
            responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					}
				},
                {
					breakpoint: 800,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
					}
				},
                {
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			],
        });

    });

</script>

<?php
    get_footer();
?>