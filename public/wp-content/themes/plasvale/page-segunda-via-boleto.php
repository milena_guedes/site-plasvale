<?php
    wp_enqueue_style('css_segunda_via', get_stylesheet_directory_uri().'/src/css/segunda-via.min.css', array(), null, false);
    
    get_header();
?>

<div class="container-total" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <div class="container-formulario-pai">
        <div class="container-padrao">
            <div class="container-inicial">
                <img src="<?= get_field('logo_plasvale_branca', 'geral'); ?>" alt="Logo Branca Plasvale">
                <h1><?= get_field('titulo'); ?></h1>
            </div>
            <div class="container-formulario">
                <a href="/" class="botao-voltar">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/right-arrow.svg" alt="Seta">
                    <p><?= get_field('texto_voltar'); ?></p>
                </a>
                <div class="container-campos">
                    <?php
                    
                        $todos_campos = get_field('campos_formulario'); 

                        for( $i = 0; $i < count( $todos_campos ); $i++ ){

                            $campo = $todos_campos[$i];

                    ?>
                    <label for="<?= $campo['nome']; ?>"><?= $campo['nome']; ?></label>
                    <input type="text" name="<?= $campo['nome']; ?>" required hc-mail-message placeholder="<?= $campo['placeholder']; ?>">
                    <?php
                    
                        }
                    
                    ?>
                </div>
                <div class="recapcha-google"></div>
                <div class="botao">
                    <p><?= get_field('texto_botao'); ?></p>
                </div>
                <div class="container-redes">
                    <?php
                    
                        $todas_redes = get_field('todas_as_redes', 'geral');

                        for( $i = 0; $i < count( $todas_redes ); $i++ ){                        

                            $rede = $todas_redes[$i];
                    
                    ?>

                    <a href="<?= $rede['url']; ?>" target="_blank" rel="noopener noreferrer"><img src="<?= $rede['icone_verde']; ?>" alt="Ícone <?= $rede['nome_da_rede']; ?>"></a>

                    <?php                
                        }
                    ?>
                </div>
            </div>
        </div>
        
    </div>

</div>

<?php
    get_footer();
?>