<?php
    wp_enqueue_style('css_pesquisa', get_stylesheet_directory_uri().'/src/css/pesquisa.min.css', array(), null, false);

    get_header();

    empty( get_query_var('item-pesquisado') ) ? '' : $pesquisa = get_query_var('item-pesquisado');

?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <h1><?= get_field('titulo'); ?></h1>
    <div class="container-texto">
        <?= get_field('texto'); ?>
    </div>
</div>

<div class="container-resultados-pesquisa">
    <div class="container-pequeno">
        <?php
                
            $argumentos = array(
                'post_type' => array( 'produtos', 'post' ),
                's' => $pesquisa,
            );

            $resultados = get_posts( $argumentos );

            if( !empty( $resultados ) ){
                for( $i = 0; $i < count( $resultados ); $i ++ ){
                    $resultado = $resultados[$i]
        ?>
                <a href="<?= get_permalink( $resultado->ID ); ?>"><h1><?= get_the_title( $resultado->ID ); ?></h1></a>
                
                <?php

                }
            }else{?>

                <p><?= get_field('texto_resultado_nao_encontrado'); ?></p>
            <?php
            }
            
        ?>
        
            
    </div>
</div>


<?php get_footer(); ?>