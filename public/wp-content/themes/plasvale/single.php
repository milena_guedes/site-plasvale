<?php

    wp_enqueue_style('css_single_noticia', get_stylesheet_directory_uri().'/src/css/single.min.css', array(), null, false);

    wpb_set_post_views(get_the_ID());

    get_header();

?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <h1><?= get_field('titulo'); ?></h1>
</div>

<div class="container-noticia">

    <div class="container-pequeno">
        <div class="container-sobre-noticia">

            <?php

                $term_list = wp_get_object_terms($post->ID, 'category');

                $categoria = $term_list[0]->name;

                $idioma = pll_the_languages( array( 'raw' => 1 ) );

                if( $idioma['pt']['current_lang'] == true ){ 

                    $texto_compartilhar = 'Compartilhar';

                    $texto_pesquisar = 'Buscar';

                    $texto_mais_populares = 'Mais Populares';
                }

                if( $idioma['en']['current_lang'] == true ){ 

                    $texto_compartilhar = 'Share';		

                    $texto_pesquisar = 'Search';

                    $texto_mais_populares = 'Most Popular';
                }

                if( $idioma['es']['current_lang'] == true ){ 

                    $texto_compartilhar = 'Compartir';

                    $texto_pesquisar = 'Buscar';

                    $texto_mais_populares = 'Más popular';
                }

            ?>

            <header>
                <div class="itens">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/interface.svg" alt="Icone Calendário" class="icone">
                    <p><?php the_time( 'd F Y' ); ?></p>
                </div>

                <div class="itens">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/clocks.svg" alt="Icone Relógio" class="icone">
                    <p><?= the_time( ); ?></p>
                </div>

                <div class="itens">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/tag.svg" alt="Icone Etiqueta" class="icone">
                    <p><?= $categoria; ?></p>
                </div>

                <div class="itens" id="botao-compartilhar">
                    <div class="botao">
                        <img src="<?= get_stylesheet_directory_uri() ?>/img/social-media.svg" alt="Icone <?= $texto_compartilhar; ?>" class="icone">
                        <p><?= $texto_compartilhar; ?></p>
                    </div>
                   
                    <div class="dropdown-redes" id="dropdown-redes">

                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink( $post->ID ); ?>" target="_blank">
                            <img src="<?= get_stylesheet_directory_uri() ?>/img/facebook.png" alt="Ícone Facebook">
                        </a>

                        <a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"></a>
                            
                    </div>
                    
                </div>
            </header>

            <div class="container-conteudo-noticia">
                <?= get_field('texto'); ?>
            </div>

            <div class="container-outras-noticias">
                <a href="<?= get_permalink( get_adjacent_post(false,'',true) ); ?>" class="container-outra-noticia container-anterior">
                    <?php

                        $post_anterior = get_adjacent_post(false,'',true);

                        $resumo = transformToMediumText( get_field('texto', $post_anterior) );

                    ?>
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/seta.png" alt="Ícone Seta">
                    <div class="container-conteudo">
                        <h1><?= get_field('titulo', $post_anterior); ?></h1>
                        <div class="resumo"><?= $resumo; ?></div>
                        <div class="botao"><?= get_field('texto_botao'); ?></div>
                    </div>                
                </a>
                <a href="<?= get_permalink( get_adjacent_post(false,'',false) ); ?>" class="container-outra-noticia container-proximo">
                    <?php
                    
                        $proximo_post = get_adjacent_post(false,'',false);

                        $resumo = transformToMediumText( get_field('texto', $proximo_post) );
                    
                    ?>     
                    <div class="container-conteudo">
                        <h1><?= get_field('titulo', $proximo_post); ?></h1>
                        <div class="resumo"><?= $resumo; ?></div>
                        <div class="botao"><?= get_field('texto_botao'); ?></div> 
                    </div>  
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/seta.png" alt="Ícone Seta" class="seta-proxima">              
                </a>
            </div>

        </div>    
        <?php
        
        
        ?>
        <aside class="barra-lateral">
            <div class="pesquisa">
                <input type="text" placeholder="<?= $texto_pesquisar; ?>" class="input-pesquisa" id="barra-pesquisa-post">
                <button type="button" id="iniciar-pesquisar" class="centralizar">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/tools-and-utensils.svg" alt="Ícone pesquisa">
                 </button>
            </div>
            <header>
                <img src="<?= get_stylesheet_directory_uri() ?>/img/bookmark.svg" alt="Ícone Marcador">
                <p><?= $texto_mais_populares; ?></p>
            </header>
            <?php
            
                $postpopulares = array( 

                    'post_type' => 'post',
                    'posts_per_page' => 4, 
                    'meta_key' => 'wpb_post_views_count', 
                    'orderby' => 'meta_value_num', 
                    'order' => 'DESC'

                ) ;

                $noticiasPopulares = get_posts( $postpopulares );

                for( $i = 0; $i < count( $noticiasPopulares ); $i ++ ){

                    $noticia = $noticiasPopulares[$i];

                    $noticia_id = $noticia->ID;

                    $resumo = transformToSmallText( get_field('texto', $noticia_id) );

                    $imagem = get_the_post_thumbnail_url($noticia_id);
            
            ?>
            <a href="<?= get_permalink( $noticia_id ); ?>" class="container-card-noticia">
                <div class="container-imagem" style="background-image: url('<?= $imagem; ?>'); "></div>
                <div class="container-conteudo">
                    <div class="itens">
                        <div class="item">
                            <img src="<?= get_stylesheet_directory_uri() ?>/img/interface-green.svg" alt="Ícone calendário">
                            <p><?= the_time( 'd F Y', $noticia_id ); ?></p>
                        </div>
                        <div class="item">
                            <img src="<?= get_stylesheet_directory_uri() ?>/img/clocks-green.svg" alt="Ícone relógio">
                            <p><?= get_the_time($format = '', $noticia_id); ?></p>
                        </div>                        
                    </div>
                    <div class="resumo"><?= $resumo; ?></div>
                    <div class="botao"><?= get_field('texto_botao', $noticia_id ); ?></div>
                </div>
            </a>
            <?php

                }
            
            ?>
        </aside>    
    </div>    
</div>

<script>

	jQuery(document).ready(function ($) {
		
		$('#botao-compartilhar').mouseenter( function(){ $('#dropdown-redes').css("opacity", "1"); });

		$('#dropdown-redes').mouseenter( function(){ $('#dropdown-redes').css("opacity", "1"); });

		$('#botao-compartilhar').mouseleave( function(){ $('#dropdown-redes').css("opacity", "0"); });

		$('#dropdown-redes').mouseleave( function(){ $('#dropdown-redes').css("opacity", "0"); });

        $('#iniciar-pesquisar').click( function(){ 

            window.location.href = '<?php echo home_url();?>/pesquisa/' + document.getElementById("barra-pesquisa-post").value; 
            
        });

	});

    jQuery("#barra-pesquisa-post").on('keyup', function (e) {
		console.log('key up');
		if (e.keyCode == 13) {
			console.log('key up');
			window.location.href = '<?php echo home_url();?>/pesquisa/' + document.getElementById("barra-pesquisa-post").value;
		}
	});
	
</script>

<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>

<?php get_footer();?>