<?php

	wp_enqueue_style('css_home', get_stylesheet_directory_uri().'/src/css/front-page.min.css', array(), null, false);
	get_header();
?>

<div class="carrossel-inicial-pai">
    <div class="carrossel-inicial">
        <?php
            $carrossels = get_field('carrossel_inicial', $post->ID);

            $contadorCarrosel = count($carrossels);

            for( $i = 0; $i < $contadorCarrosel; $i ++ ){

                $carrossel = $carrossels[$i];

                $imagem = $carrossel['imagem_de_fundo'];
        ?>
        <div class="container-carrossel" style="background-image: url('<?= $imagem; ?>');">
            <div class="container-banner"> 
                <div class="setas">
                    <div class="seta seta-anterior"> 
                        <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-left-solid.png">  
                    </div>
                    <div class="container-conteudo">                                                                    
                        <h1 class="texto-grande"><?= $carrossel['titulo']?></h1>
                        <p class="descricao"><?= $carrossel['texto']?></p>
                    </div>
                    <div class="seta seta-proxima">
                        <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-right-solid.png">  
                    </div>
                </div>                
            </div> 
        </div>
        <?php } ?>
    </div>
</div>

<div class="sessao-video">
    <div class="container-medio">
        <div class="container-video">
            <?php
                $chave = get_field('sessao_video')['url_video_yt'];            
            ?>
            <iframe src="https://www.youtube.com/embed/<?= $chave . '?controls=0'; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="container-texto">
            <div class="texto">
                <?= get_field('sessao_video')['texto']; ?>
            </div>            
        </div>
    </div>
</div>

<div class="container-ultimos-lancamentos">
    <div class="container-pequeno">
        <h1><?= get_field('sessao_ultimos_lancamentos')['titulo']; ?></h1>
        <div class="carrossel-lancamentos-pai">
        
            <div class="seta seta-anterior-lancamentos"> 
                <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-left-solid.png">  
            </div>
          
            <div class="carrossel-lancamentos-filho">
                <?php
                
                    $argumentos = array(
                        'post_type' => 'produtos',
                        'posts_per_page' => 8,
                        'orderby' => 'date',
                        'order' => 'DESC'                               
                    );

                    $produtos = get_posts($argumentos);

                    for( $i = 0; $i < count( $produtos ); $i ++ ){

                        $produto = $produtos[$i];

                    ?>

                    <a href="<?= get_permalink( $produto->ID ); ?>" class="card-produto">                       
                        <img src="<?= get_field('imagens_do_produto', $produto->ID)[0]['imagem']; ?>" alt="<?= 'Imagem do produto ' . $produto->post_title; ?>">
                        <h2><?= $produto->post_title; ?></h2>
                        <p><?= get_field('ref', $produto->ID); ?></p>
                    </a>

                    <?php

                    }

                ?>
            
            </div>
            
            <div class="seta seta-proxima-lancamentos">
                <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-right-solid.png">  
            </div>
        
        </div>
    </div>            
</div>

<div class="container-nossos-produtos">
    <div class="cabecalho">
        <h1><?= get_field('sessao_produtos')['titulo']; ?></h1>
        <p><?= get_field('sessao_produtos')['sub-titulo']; ?></p>
    </div>
    <div class="container-categoria-produtos">
        <div class="container-pequeno">
            <?php
            
                $todas_categorias = get_field('sessao_produtos')['todos_os_produtos'];

                for( $i = 0; $i < count( $todas_categorias ); $i ++ ){

                    $categoria = $todas_categorias;

                ?>

                <a href="/produtos/<?= $categoria[$i]['slug'] . '/'; ?>" class="card-categoria" style="background-color: <?= $categoria[$i]['cor_do_fundo']; ?>">
                    <img src="<?= $categoria[$i]['icone']; ?>" alt="Imagem da categoria <?= $categoria[$i]['nome']; ?>">
                    <p><?= $categoria[$i]['nome']; ?></p>
                </a>

                <?php

                }
            
            ?>
        </div>    
    </div>
    <div class="container-botao">
        <div class="container-pequeno">
            <a href="/produtos" class="botao"><?= get_field('sessao_produtos')['texto_botao']; ?></a>
        </div>        
    </div>
</div>

<div class="container-redes-sociais">
    <div class="container-medio">
        <div class="titulo"><?= get_field('sessao_redes_sociais')['titulo']; ?></div>
        <div class="todas-redes">
            <div class="card card-facebook" style="background-image: url('<?= get_field('sessao_redes_sociais')['facebook']['imagem_de_fundo']; ?>'); ">
                <div class="card-conteudo">
                    <div class="texto"><?= get_field('sessao_redes_sociais')['facebook']['texto']; ?></div>
                    <a href="/" class="botao">
                        <p><?= get_field('sessao_redes_sociais')['facebook']['texto_botao']; ?></p>                     
                        <img src="<?= get_field('sessao_redes_sociais')['facebook']['logo_colorida']; ?>" alt="Logo Youtube">
                    </a>
                </div>                
            </div>
            <div class="card card-instagram" style="background-image: url('<?= get_field('sessao_redes_sociais')['instagram']['imagem_de_fundo']; ?>'); ">
                <div class="card-conteudo">
                    <div class="texto"><?= get_field('sessao_redes_sociais')['instagram']['texto']; ?></div>
                    <a href="/" class="botao">
                        <p><?= get_field('sessao_redes_sociais')['instagram']['texto_botao']; ?></p>
                        <img src="<?= get_field('sessao_redes_sociais')['instagram']['logo_branca']; ?>" alt="Logo Instagram">
                    </a>
                </div>
            </div>
            <div class="card card-youtube" style="background-image: url('<?= get_field('sessao_redes_sociais')['youtube']['imagem_de_fundo']; ?>'); ">
                <div class="card-conteudo">
                    <div class="texto"><?= get_field('sessao_redes_sociais')['youtube']['texto']; ?></div>
                    <a href="/" class="botao">
                        <p><?= get_field('sessao_redes_sociais')['youtube']['texto_botao']; ?></p>
                        <img src="<?= get_field('sessao_redes_sociais')['youtube']['logo_branca']; ?>" alt="Logo Youtube" id="logo-branca-youtube">
                        <img src="<?= get_field('sessao_redes_sociais')['youtube']['logo_colorida']; ?>" alt="Logo Youtube" id="logo-colorida-youtube">
                    </a>
                </div>
            </div>
            <div class="card card-pinterest" style="background-image: url('<?= get_field('sessao_redes_sociais')['pinterest']['imagem_de_fundo']; ?>'); ">
                <div class="card-conteudo">
                    <div class="texto"><?= get_field('sessao_redes_sociais')['pinterest']['texto']; ?></div>
                    <a href="/" class="botao">
                        <p><?= get_field('sessao_redes_sociais')['pinterest']['texto_botao']; ?></p>
                        <img src="<?= get_field('sessao_redes_sociais')['pinterest']['logo_branca']; ?>" alt="Logo Pinterest">
                    </a>
                </div>    
            </div>
        </div>
    </div>
</div>

<div class="container-ultimas-noticias">
    <div class="container-padrao">
        <h1><?= get_field('sessao_ultimas_noticias')['titulo']; ?></h1>
        <div class="container-noticias">
            <?php

                $argumentos = array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'orderby' => 'date',
                    'order' => 'DESC'                               
                );

                $cards_noticias = get_posts($argumentos);


                for( $i = 0; $i < count( $cards_noticias ); $i ++ ){

                    $noticia = $cards_noticias[$i];
                    
                    $texto_botao = get_field('sessao_ultimas_noticias')['texto_botao'];
                             
            ?>
            <a href="<?= get_permalink( $noticia->ID ); ?>" class="container-card-noticia">
                <div class="imagem" style="background-image: url('<?= get_the_post_thumbnail_url( $noticia->ID ); ?>'); "></div>
                <h1><?= $noticia->post_title; ?></h1>
                <div class="breve-resumo">
                    <?= transformToMediumText( get_field( 'texto', $noticia->ID ) ); ?>
                </div>
                <div class="botao"><p><?= $texto_botao; ?></p></div>
            </a>
            <?php
            
                }
            
            ?>
        </div>
    </div>

</div>

<div class="container-todos-depoimentos">
    <div class="container-pequeno">
        <h1><?= get_field('sessao_depoimentos')['titulo'];  ?></h1>
        
        <div class="container-carrossel-pai">
            <?php
                        
                $todos_depoimentos = get_field('sessao_depoimentos')['todos_os_depoimentos']; 

                for( $i = 0; $i < count( $todos_depoimentos ); $i ++ ){

                    $depoimento = $todos_depoimentos[$i];

            ?>
            <div class="container-carrossel-filho">

                <div class="container-frase-cliente">

                    <div class="icone-aspas">
                        <img src="<?=get_stylesheet_directory_uri()?>/img/aspas.png">
                    </div>

                    <div class="texto">
                        <?= $depoimento['texto']; ?>
                    </div>

                    <div class="icone-seta">
                        <img src="<?=get_stylesheet_directory_uri()?>/img/triangulo.png">
                    </div>

                </div>

                <div class="container-pos-frase">

                    <div class="container-setas">
                        <div class="seta seta-anterior">
                            <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-left-solid-white.png">
                        </div>
                        <div class="seta seta-proxima">
                            <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-right-solid-white.png">
                        </div>
                    </div>

                    <div class="container-sobre-cliente">
                        <h2><?=  $depoimento['nome_cliente']; ?></h2>
                        <p><?=  $depoimento['ano_do_depoimento']; ?></p>
                    </div>

                    <div class="container-vazio"></div>

                </div>
                
            </div>

            <?php
        
                }
            
            ?>
        </div>
        
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function ($) {

        $('.carrossel-inicial').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            infinite: true,
            fade: true,
            prevArrow: $('.setas .seta-anterior'),
            nextArrow: $('.setas .seta-proxima')
        });

        $('.container-carrossel-pai').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: true,
            prevArrow: $('.container-setas .seta-anterior'),
            nextArrow: $('.container-setas .seta-proxima')
        });
        
        $('.carrossel-lancamentos-filho').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: false,
            prevArrow: $('.seta-anterior-lancamentos'),
            nextArrow: $('.seta-proxima-lancamentos'),
            responsive: [
				{
					breakpoint: 1180,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					}
				},
                {
					breakpoint: 1000,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
					}
				},
                {
					breakpoint: 800,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			],
        });
    });

</script>

<?php get_footer(); ?>