<?php  do_action('before_wphead_after_init');?>
<!DOCTYPE html>
<html <?php language_attributes();?> class="no-js">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics 
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-35969453-1"></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'UA-35969453-1');
    </script>
    -->

    <?php
    	//HC::init();
    ?>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0" />
    <title><?php echo wp_title();?></title>

    <!-- ESTILOS -->
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/header.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/footer.min.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/src/css/global.min.css">
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/js/plugins/slick/slick.css"/>
	<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/js/plugins/slick/slick-theme.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- FONTES -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@300&display=swap" rel="stylesheet">

    <?php wp_head();?>
</head>
<body class="scroll-body scroll-block" id="body">

<!-- <div class="container-loading" id="container-loading">
	<div class="conteudo loading">
		<img id="logo" src="<?=get_stylesheet_directory_uri()?>/img/logo_elar_preto.png">
		<img id="gif" src="<?=get_stylesheet_directory_uri()?>/img/loading.gif">
	</div>
</div> -->
<?php

	$header = get_field('cabecalho_principal', 'header');

	$idioma = pll_the_languages( array( 'raw' => 1 ) );					

	if( $idioma['pt']['current_lang'] == true ){ 

		$itens_menu = $header['pt_menu']['itens_menu'];

		$botao_menu = $header['pt_menu']['botao_segunda_via'];

		$categorias = $header['pt_menu']['itens_subcategoria_produto'];

		$id_pagina_produto = 683;

		$texto_pesquisa = $header['pt_menu']['texto_placeholder_pesquisa'];
	}

	if( $idioma['en']['current_lang'] == true ){ 

		$itens_menu = $header['en_menu']['itens_menu'];

		$botao_menu = $header['en_menu']['botao_segunda_via'];

		$categorias = $header['en_menu']['itens_subcategoria_produto'];

		$id_pagina_produto = 685;

		$texto_pesquisa = $header['en_menu']['texto_placeholder_pesquisa'];

	}

	if( $idioma['es']['current_lang'] == true ){ 

		$itens_menu = $header['es_menu']['itens_menu'];		
		
		$botao_menu = $header['es_menu']['botao_segunda_via'];

		$categorias = $header['es_menu']['itens_subcategoria_produto'];

		$id_pagina_produto = 687;

		$texto_pesquisa = $header['es_menu']['texto_placeholder_pesquisa'];

	}

?>
<header class="header">
	<div class="menu-lateral" id="menu-lateral">
		<div class="after-container">
			<div class="container-padrao">
				<div class="container-menu">
					<img onclick="abrirMenu()" src="<?= get_stylesheet_directory_uri()?>/img/cross-black-circular-button.svg">
				</div>
				<div class="container-logo">
					<a href="/"><img src="<?= get_field('logo_plasvale_branca', 'geral'); ?>" alt="Logo Plasvale"></a>
				</div>				
			</div>
			<div class="itens-responsivo">
				<ul>
					<?php 

						for ( $i = 0; $i < count( $itens_menu ); $i ++ ) {

							$link = $itens_menu[$i]['url'];

							$nome = $itens_menu[$i]['nome'];
					
					?>

					<li class="item">
						<a href="<?= get_home_url() . $link; ?>">
							<?= $nome; ?>
						</a>
					</li>

					<?php 
						}
					?>
					<li class="botao-lightbox"><p><?= get_field('texto_botao_lightbox', 'header'); ?></p></li>
				</ul>
				<a href="<?= $botao_menu['url']; ?>" class="botao-menu"><p><?= $botao_menu['texto']; ?></p></a>
				<span class="container-pesquisa">
					<input type="text" placeholder="<?= $texto_pesquisa; ?>" id="barra-pesquisa-responsive">
					<img src="<?=get_stylesheet_directory_uri()?>/img/loupe.svg" id="lupa-responsive">
				</span>
			</div>
		</div>
	</div>
	<div class="container-total">		
		<div class="container-inicial-desktop">
			<div class="container-medio">
				<section class="seletor-idioma-pai">
					<section class="seletor-idioma" id="seletor-idioma">
						<img src="<?= get_stylesheet_directory_uri() ?>/img/globe.png" alt="Globo">					
					</section>
					<div class="opcoes" id="opcoes-idioma">
						<a href="/" class="opcao divisor">PT</a>
						<a href="/" class="opcao divisor">ES</a>
						<a href="/" class="opcao">EN</a>
					</div>
					<div class="mensagem">
						<p><?= get_field('seletor_idioma', 'header')['texto_aviso']; ?></p>
						<img src="<?= get_stylesheet_directory_uri() ?>/img/cancel.png" id="botao-fechar" alt="Botão Fechar">
					</div>
				</section>
				<section class="barra-pesquisa">
					<input type="text" placeholder="<?= $texto_pesquisa; ?>" id="barra-pesquisa">
				</section>
				<section class="redes">
					<div class="lupa-pesquisa">						
						<img id="abrir-pesquisa" src="<?=get_stylesheet_directory_uri()?>/img/search.png">
						<span id="fechar-pesquisa" class="close">&times;</span>
					</div>
					<div class="redes-sociais">
						<?php

							$todas_redes = get_field('todas_as_redes', 'geral');

							for( $i = 0; $i < count( $todas_redes ); $i ++ ){

								$rede = $todas_redes[$i];

								$nome = $rede['nome_da_rede'];

								$url = $rede['url'];

								$icone = $rede['icone_preta'];

							?>

							<a href="<?= $url; ?>"><img src="<?= $icone; ?>" alt="<?= $nome; ?>" class="logo"></a>

							<?php

							}
						
						?>
					</div>
				</section>
			</div>
		</div>				
		<div class="container-desktop">
			<div class="container-medio">
				<a href="/"><img src="<?= get_field('logo_principal', 'geral'); ?>" alt="Logo Plasvale" class="logo-principal"></a>

				<?php 

					for ($i = 0; $i < count( $itens_menu ); $i ++) {

						$link = $itens_menu[$i]['url'];

						$nome = $itens_menu[$i]['nome'];	

						$id_item = $itens_menu[$i]['id_item'];	

						$item_ativo = is_front_page() || is_home() ? '' : $post->post_name;
						
						if( $link == $item_ativo ){

							$classe_ativa = 'active';

						}else{
							
							$classe_ativa = '';
						}
				?>
				
				<div class="item-pai" id="<?= $id_item; ?>">
					<a href="<?= get_home_url() . $link; ?>" class="item <?= $classe_ativa; ?>"><p><?= $nome; ?></p></a>
					<?php if( $id_item == 'nossos-produtos' ){ ?>
						<div class="container-categorias-produtos">
							<?php

								for ($j = 0; $j < count( $categorias ); $j ++) {

									$categoria = $categorias[$j]['categoria'];

									$url = get_permalink($id_pagina_produto) . 'categoria/'. $categoria->slug;

									empty($cor) ? $cor = '#abd6cb' : $cor = get_field('cor_de_fundo', 'categoria' . '_' . $categoria->term_id);

									$nome = $categoria->name;

									$icone = get_field('icone_preto', 'categoria' . '_' . $categoria->term_id);
							
							?>
							<a href="<?= $url; ?>" class="item-cat" style="border-right: 6px solid <?= $cor; ?>;" 
								onMouseOver="this.style.backgroundColor='<?= $cor; ?>'" 
								onMouseOut="this.style.backgroundColor='#ffffff'">
								<img src="<?= $icone; ?>" alt="Ícone categoria <?= $nome; ?>">
								<p><?= $nome; ?></p>
							</a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
				<?php } ?>

				<a href="<?= $botao_menu['url']; ?>" class="botao-menu"><p><?= $botao_menu['texto']; ?></p></a>
			</div>
			
		</div>
		<div class="container-resposivo">
			<div class="container-logo">
				<a href="/"><img src="<?= get_field('logo_principal', 'geral'); ?>" alt="Logo Plasvale"></a>
			</div>
			<div class="container-menu">
				<img onclick="abrirMenu()" src="<?= get_stylesheet_directory_uri()?>/img/menu.svg">
			</div>				
		</div>		
	</div>

	<script>

		jQuery("#barra-pesquisa").on('keyup', function (e) {
			console.log('key up');
			if (e.keyCode == 13) {
				console.log('key up');
				window.location.href = '<?php echo home_url();?>/pesquisa/' + document.getElementById("barra-pesquisa").value;
			}
		});
		
		jQuery(document).ready(function ($) {

			$('#nossos-produtos').mouseenter( function(){
				$('.container-categorias-produtos').css("opacity", "1"); 

				$('.container-categorias-produtos').css("display", "block"); 
			});

			$('#nossos-produtos').mouseleave( function(){ 
				
				$('.container-categorias-produtos').css("opacity", "0");

				$('.container-categorias-produtos').css("display", "none"); 
				
			});


			$('.container-categorias-produtos').mouseleave( function(){ 
				
				$('.container-categorias-produtos').css("opacity", "0");

				$('.container-categorias-produtos').css("display", "none"); 
				
			}); 


			$('#abrir-pesquisa').click( function(){ 

				$('#barra-pesquisa').css("width", "100%"); 

				$('#abrir-pesquisa').css("opacity", "0"); 

				$('#fechar-pesquisa').css("opacity", "1"); 

			});

			$('#fechar-pesquisa').click( function(){ 

				$('#barra-pesquisa').css("width", "0"); 

				$('#abrir-pesquisa').css("opacity", "1"); 

				$('#fechar-pesquisa').css("opacity", "0"); 

			});

		
			$('#seletor-idioma').mouseenter( function(){ $('#opcoes-idioma').css("opacity", "1"); });

			$('#opcoes-idioma').mouseenter( function(){ $('#opcoes-idioma').css("opacity", "1"); });

			$('#seletor-idioma').mouseleave( function(){ $('#opcoes-idioma').css("opacity", "0"); });

			$('#opcoes-idioma').mouseleave( function(){ $('#opcoes-idioma').css("opacity", "0"); });

			$('#botao-fechar').click( function(){ $('.mensagem').css("opacity", "0"); });


			$('#lupa-responsive').click( function(){ 
				window.location.href = '<?php echo home_url();?>/pesquisa/' + document.getElementById("barra-pesquisa-responsive").value;
			});

		});
	
	</script>
	
</header>
