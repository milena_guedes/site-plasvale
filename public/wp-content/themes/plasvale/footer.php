<?php

	wp_enqueue_style('css_footer', get_stylesheet_directory_uri().'/src/css/footer.min.css', array(), null, false);

	$idioma = pll_the_languages( array( 'raw' => 1 ) );

	if( $idioma['pt']['current_lang'] == true ){ 
	
		$imagem_fundo = get_field('campos_newsletter', 'footer')['pt']['imagem_de_fundo']; 

		$titulo = get_field('campos_newsletter', 'footer')['pt']['titulo']; 

		$descricao = get_field('campos_newsletter', 'footer')['pt']['descricao']; 

		$texto_input = get_field('campos_newsletter', 'footer')['pt']['texto_input']; 

		$texto_botao = get_field('campos_newsletter', 'footer')['pt']['texto_botao']; 

		$texto_marca = get_field('campos_ultimo_bloco', 'footer')['pt']['texto_marcas']; 

		$texto_ultimo = get_field('campos_ultimo_bloco', 'footer')['pt']['texto']; 
	
	}		

	if( $idioma['en']['current_lang'] == true ){ 

		$imagem_fundo = get_field('campos_newsletter', 'footer')['en']['imagem_de_fundo']; 

		$titulo = get_field('campos_newsletter', 'footer')['en']['titulo']; 

		$descricao = get_field('campos_newsletter', 'footer')['en']['descricao']; 

		$texto_input = get_field('campos_newsletter', 'footer')['en']['texto_input']; 

		$texto_botao = get_field('campos_newsletter', 'footer')['en']['texto_botao']; 

		$texto_marca = get_field('campos_ultimo_bloco', 'footer')['en']['texto_marcas']; 

		$texto_ultimo = get_field('campos_ultimo_bloco', 'footer')['en']['texto']; 
		
	}

	if( $idioma['es']['current_lang'] == true ){ 

		$imagem_fundo = get_field('campos_newsletter', 'footer')['en']['imagem_de_fundo']; 

		$titulo = get_field('campos_newsletter', 'footer')['es']['titulo']; 

		$descricao = get_field('campos_newsletter', 'footer')['es']['descricao']; 

		$texto_input = get_field('campos_newsletter', 'footer')['es']['texto_input']; 

		$texto_botao = get_field('campos_newsletter', 'footer')['es']['texto_botao']; 

		$texto_marca = get_field('campos_ultimo_bloco', 'footer')['es']['texto_marcas']; 

		$texto_ultimo = get_field('campos_ultimo_bloco', 'footer')['es']['texto']; 
		
	}

?>

<div class="receba-novidades centralizar" style="background-image: url('<?= $imagem_fundo; ?>'); ">
    <div class="container-padrao">  

        <div class="textos anime anime-right">
            <h1><?= $titulo; ?></h1>
            <p><?= $descricao; ?></p>
        </div>

        <form method="POST" action="<?php echo admin_url('admin-ajax.php');?>" data-hibrido-subscribers-form class="anime anime-right">
            <input id="email" type="text" name="email" placeholder="<?= $texto_input; ?>">
			<button type="submit"><?= $texto_botao; ?></button>
			<p data-hibrido-subscribers-response></p>
        </form>

    </div>
</div>

<?php

	$idioma = pll_the_languages( array( 'raw' => 1 ) );

	$header = get_field('cabecalho_principal', 'header');	

	if( $idioma['pt']['current_lang'] == true ){ 

		$pagina_produtos = 'produtos';

		$itens_menu = $header['pt_menu']['itens_menu'];

		$itens_categoria_produto = $header['pt_menu']['itens_subcategoria_produto'];


		$todos_contatos = get_field('informacoes_de_contato', 'geral')['pt_campos']['nums_telefone']['todos_os_numeros'];

		$campos_email = get_field('informacoes_de_contato', 'geral')['pt_campos']['e-mail'];


		$titulo_navegacao = get_field('campos_footer', 'footer')['pt']['titulo_bloco_navegacao'];


		$titulo_produtos = get_field('campos_footer', 'footer')['pt']['titulo_bloco_produtos'];


		$titulo_contato = get_field('campos_footer', 'footer')['pt']['titulo_bloco_contato'];


		$campos_endereco = get_field('informacoes_de_contato', 'geral')['pt_campos']['endereco'];


		$campos_atendimento = get_field('informacoes_de_contato', 'geral')['pt_campos']['horario_de_funcionamento'];


		$titulo_redes = get_field('campos_footer', 'footer')['pt']['titulo_bloco_redes_sociais'];

		$id_pagina_produto = 683;
	}

	if( $idioma['en']['current_lang'] == true ){ 

		$pagina_produtos = 'products';

		$itens_menu = $header['en_menu']['itens_menu'];

		$itens_categoria_produto = $header['en_menu']['itens_subcategoria_produto'];

		$todos_contatos = get_field('informacoes_de_contato', 'geral')['en_campos']['nums_telefone']['todos_os_numeros'];

		$campos_email = get_field('informacoes_de_contato', 'geral')['en_campos']['e-mail'];

		$titulo_navegacao = get_field('campos_footer', 'footer')['en']['titulo_bloco_navegacao'];

		$titulo_produtos = get_field('campos_footer', 'footer')['en']['titulo_bloco_produtos'];

		$titulo_contato = get_field('campos_footer', 'footer')['en']['titulo_bloco_contato'];

		$campos_endereco = get_field('informacoes_de_contato', 'geral')['en_campos']['endereco'];

		$campos_atendimento = get_field('informacoes_de_contato', 'geral')['en_campos']['horario_de_funcionamento'];

		$titulo_redes = get_field('campos_footer', 'footer')['en']['titulo_bloco_redes_sociais'];

		$id_pagina_produto = 685;
	}

	if( $idioma['es']['current_lang'] == true ){ 

		$pagina_produtos = 'produtos-en';

		$itens_menu = $header['es_menu']['itens_menu'];	

		$itens_categoria_produto = $header['es_menu']['itens_subcategoria_produto'];

		$todos_contatos = get_field('informacoes_de_contato', 'geral')['es_campos']['nums_telefone']['todos_os_numeros'];

		$campos_email = get_field('informacoes_de_contato', 'geral')['es_campos']['e-mail'];
		
		$titulo_navegacao = get_field('campos_footer', 'footer')['es']['titulo_bloco_navegacao'];	
		
		$titulo_produtos = get_field('campos_footer', 'footer')['es']['titulo_bloco_produtos'];

		$titulo_contato = get_field('campos_footer', 'footer')['es']['titulo_bloco_contato'];

		$campos_endereco = get_field('informacoes_de_contato', 'geral')['es_campos']['endereco'];

		$campos_atendimento = get_field('informacoes_de_contato', 'geral')['es_campos']['horario_de_funcionamento'];		

		$titulo_redes = get_field('campos_footer', 'footer')['es']['titulo_bloco_redes_sociais'];

		$id_pagina_produto = 687;
		
	}

?>

<footer>
	<div class="container-conteudo">
		<div class="container-medio">
			<div class="container-inicial">
				<img src="<?= get_field('logo_plasvale_branca', 'geral'); ?>" alt="Logo Plasvale">
				<div class="container-idiomas">
					<a href="/">PT</a>
					<a href="/">EN</a>
					<a href="/">ES</a>
				</div>
			</div>
			<div class="container-navegacao">
				<h1><?= $titulo_navegacao; ?></h1>
				<div class="lista-itens">
					<?php
					
						for ($i = 0; $i < count( $itens_menu ); $i ++) {

							$link = $itens_menu[$i]['url'];
	
							$nome = $itens_menu[$i]['nome'];
					?>
					<a href="<?= get_home_url() . $link; ?>"><?= $nome; ?></a>
					<?php
					
						}
					?>
				</div>
			</div>
			<div class="container-categorias">
				<h1><?= $titulo_produtos; ?></h1>
				<div class="todas-categorias">
					<?php
						
						for ($i = 0; $i < count( $itens_categoria_produto ); $i ++) {

							$slug = $itens_categoria_produto[$i]['categoria']->slug;

							$url = get_permalink($id_pagina_produto) . 'categoria/'. $slug;

							$nome = $itens_categoria_produto[$i]['categoria']->name;
					?>
					<a href="<?= $url; ?>"><?= $nome; ?></a>
					<?php
					
						}
					?>
				</div>
			</div>
			<div class="container-contatos">
				<h1><?= $titulo_contato; ?></h1>
				<div class="todos-contatos">
					<?php
					
						for( $i = 0; $i < count( $todos_contatos ); $i ++ ){

							$icone =  $todos_contatos[$i]['icone'];

							$numero = $todos_contatos[$i]['numero'];					
					?>
					<div class="contato">
						<img src="<?= $icone; ?>" alt="Ícone contato">
						<p><?= $numero; ?></p>
					</div>
					<?php

						}
					
					?>
					<div class="email">
						<img src="<?= $campos_email['icone']; ?>" alt="Ícone email">
						<p><?= $campos_email['email']; ?></p>
					</div>
				</div>
			</div>
			<div class="container-outros-contatos">
				<h1><?= $campos_endereco['nome_do_campo']; ?></h1>
				<div class="endereco">
					<?= $campos_endereco['endereco']; ?>	
				</div>

				<h1><?= $campos_atendimento['nome_do_campo']; ?></h1>
				<div class="atendimento">
					<?= $campos_atendimento['horarios']; ?>	
				</div>

				<h1><?= $titulo_redes; ?></h1>
				<div class="redes">
					<?php
					
						$todas_redes = get_field('todas_as_redes', 'geral');

						for( $i = 0; $i < count( $todas_redes ); $i ++ ){

							$icone = $todas_redes[$i]['icone_branca'];

							$url = $todas_redes[$i]['url'];

							$nome = $todas_redes[$i]['nome_da_rede'];

					?>

					<a href="<?= $url; ?>" target="_blank" rel="noopener noreferrer" class="rede"><img src="<?= $icone; ?>" alt="<?= 'Icone da rede' . $nome; ?>"></a>
					
					<?php
					
						}	

					?>
				</div>

				<img src="<?= get_field('logo_ja_santa_catarina', 'geral'); ?>" alt="Logo JA Santa Catarina" class="logo-ja">

			</div>
			<div class="botao-inicio">
				<div class="scroll-top">
					<img src="<?= get_stylesheet_directory_uri()?>/img/chevron-left-solid.png">
					<p>TOP</p>
				</div>
			</div>
		</div>
	</div>
	
</footer>

<div class="container-ultimo centralizar">
	<div class="container-medio">
		<div class="container-nossas-marcas">
			<p><?= $texto_marca; ?></p>
			<img src="<?= get_field('logo_principal', 'geral'); ?>" alt="Logo Plasvale">
			<img src="<?= get_field('logo_attuale', 'geral'); ?>" alt="Logo Attuale">
			<img src="<?= get_field('logo_itdog', 'geral'); ?>" alt="Logo it.dog">
		</div>
		<div class="texto">
			<p><?= $texto_ultimo; ?> <a href="http://demacode.com.br" target="_blank"> Demacode_</a></p>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?= get_template_directory_uri() ?>/js/plugins/slick/slick.min.js"></script>

<script>

	$(document).ready(function(){
		$(".scroll-top").click(function() {
			$("html, body").animate({ 
				scrollTop: 0 
			}, "slow");
			return false;
		});
	});

</script>

<?php wp_footer();?>


</body>
</html>