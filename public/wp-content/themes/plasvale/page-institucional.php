<?php
    wp_enqueue_style('css_institucional', get_stylesheet_directory_uri().'/src/css/institucional.min.css', array(), null, false);

    get_header();
    
?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <h1><?= get_field('titulo'); ?></h1>
    <div class="container-texto">
        <?= get_field('texto'); ?>
    </div>
</div>

<div class="container-principios">
    <div class="container-medio">
        <?php

            $todos_cards = get_field('sessao_cards_iniciais');
        
            for( $i = 0; $i < count( $todos_cards ); $i ++ ){

                $card = $todos_cards[$i];
        
        ?>

        <div class="container-card camada-verde" style="background-image: url('<?= $card['imagem_de_fundo']; ?>'); ">
            <div class="container-card-conteudo">
            
                <h1><?= $card['titulo']; ?></h1>

                <div class="texto">
                    <?= $card['texto']; ?>
                </div>

            </div>
        </div>

        <?php

            }
        
        ?>
    </div>
</div>

<div class="sessao-video">
    <div class="container-medio">
        <div class="container-video">
            <?php
                $chave = get_field('sessao_video')['chave_do_video_no_youtube'];            
            ?>
            <iframe src="https://www.youtube.com/embed/<?= $chave . '?controls=0'; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="container-texto">
            <div class="texto">
                <?= get_field('sessao_video')['texto']; ?>
            </div>            
        </div>
    </div>
</div>

<div class="container-politica-qualidade">
    <div class="container-padrao">
        <div class="container-politicas">
            <div class="barra-lateral">
                <div class="container-inicial">
                    <h1><?= get_field('sessao_politica_de_qualidade')['titulo']; ?></h1>
                    <div class="texto">
                        <?= get_field('sessao_politica_de_qualidade')['texto']; ?>
                    </div>
                </div>                
                <div class="seta"></div>
                <div class="container-topicos-pai">
                    <?php
                    
                        $topicos = get_field('sessao_politica_de_qualidade')['topicos'];
            
                        for( $i = 0; $i < count( $topicos ); $i ++ ){
            
                            $topico = $topicos[$i];
                    
                    ?>
                    <div class="container-topico container-<?= $i + 1; ?>" referencia="<?= $i + 1; ?>">
                        <div class="seta seta-<?= $i + 1; ?>"></div>
                        <img src="<?= $topico['icone']; ?>" class="imagem-desativa imagem-desativa-<?= $i + 1; ?>">
                        <img src="<?= $topico['icone_selecionada']; ?>" class="imagem-ativa imagem-ativa-<?= $i + 1; ?>">
                        <p><?= $topico['texto_junto_icone']; ?></p>
                    </div>
                    <?php
                    
                        }

                    ?>
                </div>
            </div>
            <div class="container-imagens">
                <div class="container-imagem ativo" style="background-image: url('<?= get_field('sessao_politica_de_qualidade')['imagem_de_fundo']; ?>'); "></div>
                <?php
                
                    $topicos = get_field('sessao_politica_de_qualidade')['topicos'];
                
                    for( $i = 0; $i < count( $topicos ); $i ++ ){
        
                        $topico = $topicos[$i];
                
                ?>

                <div class="container-imagem imagem-<?= $i + 1; ?> camada-verde" style="background-image: url('<?= $topico['imagem_grande']; ?>'); ">
                    <div class="texto">
                        <?= $topico['texto_junto_imagem_grande']; ?>
                    </div>
                </div>

                <?php
                
                    }
                
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container-numeros" style="background-image: url('<?= get_field('sessao_plasvale_em_numeros')['imagem_de_fundo']; ?>'); ">
    <div class="container-padrao">
        <h1 class="titulo anime"><?= get_field('sessao_plasvale_em_numeros')['titulo']; ?></h1>
        <div class="cards-numeros-pai anime">
            <?php
            
                $cards_numeros = get_field('sessao_plasvale_em_numeros')['numeros'];

                for( $i = 0; $i < count($cards_numeros); $i ++ ){

                    $card = $cards_numeros[$i];

                    $icone = $card['icone'];

                    $nome = $card['numero'];

                    $descricao = $card['texto'];            
            ?>
            <div class="card-numero">
                <img src="<?= $icone; ?>" alt="<?= $nome; ?>">
                <div class="textos">
                    <h1><?= $nome; ?></h1>
                    <p><?= $descricao; ?></p>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="container-ultimas-noticias">
    <div class="container-padrao">
        <h1><?= get_field('sessao_ultimas_noticias')['titulo']; ?></h1>
        <div class="container-noticias">
            <?php
                
                $cards_noticias = get_field('sessao_ultimas_noticias')['todas_as_noticias'];

                for( $i = 0; $i < count( $cards_noticias ); $i ++ ){

                    $noticia = $cards_noticias[$i]['noticia'];
                    
                    $texto_botao = $cards_noticias[$i]['texto_botao'];
                             
            ?>
            <a href="<?= get_permalink( $noticia->ID ); ?>" class="container-card-noticia">
                <div class="imagem" style="background-image: url('<?= get_the_post_thumbnail_url( $noticia->ID ); ?>'); "></div>
                <h1><?= $noticia->post_title; ?></h1>
                <div class="breve-resumo">
                    <?= transformToMediumText( get_field( 'texto', $noticia->ID ) ); ?>
                </div>
                <div class="botao"><p><?= $texto_botao; ?></p></div>
            </a>
            <?php
            
                }
            
            ?>
        </div>
    </div>

</div>

<script>

	jQuery(document).ready(function ($) {
		
		$('.container-politica-qualidade .container-padrao .container-politicas .barra-lateral .container-topicos-pai .container-topico').mouseenter( function(){  
            $('.container-politica-qualidade .container-padrao .container-politicas .barra-lateral .seta').css("opacity", "0"); 

            politicasController(this.getAttribute('referencia'));

        });

        $('.container-politica-qualidade .container-padrao .container-politicas .barra-lateral .container-topicos-pai .container-topico').mouseenter( function(){  

            var seta = this.getAttribute('referencia');

            $('.container-politica-qualidade .container-padrao .container-politicas .barra-lateral .container-topicos-pai .container-topico .seta-' + seta ).css("opacity", "1"); 
        });

	});

    function politicasController(container){

        jQuery('.container-imagem').removeClass('ativo');

        jQuery('.imagem-' + container).addClass('ativo');


        jQuery('.container-topico').removeClass('ativo');

        jQuery('.container-' + container).addClass('ativo');

    }
	
</script>
 
<?php
    get_footer();
?>