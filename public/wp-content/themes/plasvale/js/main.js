var page = 2;
var url = window.origin + '/wp-admin/admin-ajax.php';
var button = jQuery('.carregar-mais');

jQuery(document).ready(function($){

	$('#container-loading').fadeOut();
	$('#body').removeClass('scroll-block');

    $target = $('.anime'),
	animationClass = 'anime-init',
	windowHeight = $(window).height(),
	offset = windowHeight - (windowHeight / 5);
	var root = document.documentElement;
	root.className += ' js';

	function boxTop(idBox) {
		var boxOffset = $(idBox).offset().top;
		return boxOffset;
	}

	function animeScroll() {
		var documentTop = $(document).scrollTop();
		$target.each(function() {
		if (documentTop > boxTop(this) - offset) {
			$(this).addClass(animationClass);
		}
		});
	}
	animeScroll();

	$(document).scroll(function() {
		$target = $('.anime');
		animeScroll();
	});

	$('.abrir-upload').on('click', function(){
		$('#arquivo').trigger('click'); 
	});

	$('#arquivo').on('change', function(){

		var callback = $('#arquivo-anexado')[0];
		callback.innerHTML = "";
		if(!this.files[0].name){
			return;
		}

		var quantidadeArquivos = this.files.length;
		for(var i = 0; i < quantidadeArquivos; i++) {
			callback.innerHTML = callback.innerHTML + this.files[i].name + "<br>";
		}

		adicionarClasse(callback, 'active');

		return;
	});
});


function abrirMenu(){
	var menuLateral = document.getElementById('menu-lateral');

	if(!menuLateral.classList.contains('mostrar-menu')) {
		menuLateral.classList.add('mostrar-menu');
			
	} else {
		menuLateral.classList.remove('mostrar-menu');
	}
}

function abrirDropDown(id){
	dropdown = document.getElementById('container-options-' + id);
	seta = document.getElementById('seta-' + id);
	if(dropdown.classList.contains('dropdown-ativo')){
		dropdown.classList.remove('dropdown-ativo');
		seta.classList.remove('dropdown-ativo');
	}else{
		dropdown.classList.add('dropdown-ativo');
		seta.classList.add('dropdown-ativo');
	}
}

function selecionarCategoria(categoria){

	dropdown = document.getElementById('container-options-' + categoria);

	seta = document.getElementById('seta-' + categoria);

	if(dropdown.classList.contains('dropdown-ativo')){

		dropdown.classList.remove('dropdown-ativo');

		seta.classList.remove('dropdown-ativo');

	}else{
		dropdown.classList.add('dropdown-ativo');

		seta.classList.add('dropdown-ativo');
	}

	alterarCategoria(categoria);
}

function filtroController(id) {
	url = removerParametro(id);
	parametro = '';

	checkBoxes = jQuery('.check-box-'+id+':checkbox:checked');
	console.log(checkBoxes);
	contadorCheckBoxes = checkBoxes.length;
	for(i = 0; i < contadorCheckBoxes; i++){
		if(checkBoxes[i].checked){
			if(i == (contadorCheckBoxes - 1)){
				parametro = parametro + checkBoxes[i].value;
			}else{
				parametro = parametro + checkBoxes[i].value + ',';
			}
		}
	}
	urlParametros = inserirParametro(id, parametro, url);

	window.location.href = urlParametros;
}

function inserirParametro(key, value, url) {
	_url = url;
	if(value != ''){
		_url += (_url.split('?')[1] ? '&':'?') + key + '=' + value;
	}
	return _url;
}

function removerParametro(key, sourceURL) {
	sourceURL = window.location.href;
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function carregarMais(categoriaQuery){
	jQuery.ajax({
		url: url,
		type: 'POST',
		async: true,
		data: {
			action: 'be_ajax_load_more',
			page: page, 
			catquery: categoriaQuery,
		},
		beforeSend: function() {
			jQuery('.carregar-mais-texto').text('Carregando...');
		},
		success: function(response){
			console.log(response);
			if(response == 'false'){
				jQuery('.carregar-mais-texto').text('Não há mais Obras');
			}else{
				var arrayPosts = JSON.parse(response);
				var countArrayPosts = arrayPosts.length;

				for(i = 0; i < countArrayPosts; i++){

					var containerDivPrincipal = document.createElement("a");
					containerDivPrincipal.className = 'container-obra-pai';
					containerDivPrincipal.href = arrayPosts[i].link;

					var containerDivSecundaria = document.createElement("div");
					containerDivSecundaria.className = 'container-obra camada-preta';
					containerDivSecundaria.style.backgroundImage = 'url(' + arrayPosts[i].imagem + ')';

					var containerDivTexto = document.createElement("div");
					containerDivTexto.className = 'container-conteudo';

					var logoObra = document.createElement("img");
					logoObra.src = arrayPosts[i].logo;

					var descricao = document.createElement("p");
					descricao.innerText = arrayPosts[i].descricao;

					var divInformacoes = document.createElement("div");
					divInformacoes.className = 'informacoes-empreendimento';

					var divInformacao1 = document.createElement("div");
					divInformacao1.className = 'informacao';

					var iconeInfo1 = document.createElement("img");
					iconeInfo1.src = arrayPosts[i].icone1;

					var descricaoInfo1 = document.createElement("p");
					descricaoInfo1.innerText = arrayPosts[i].descricao1;

					var divInformacao2 = document.createElement("div");
					divInformacao2.className = 'informacao';

					var iconeInfo2 = document.createElement("img");
					iconeInfo2.src = arrayPosts[i].icone2;

					var descricaoInfo2 = document.createElement("p");
					descricaoInfo2.innerText = arrayPosts[i].descricao2;

					var divInformacao3 = document.createElement("div");
					divInformacao3.className = 'informacao';

					var iconeInfo3 = document.createElement("img");
					iconeInfo3.src = arrayPosts[i].icone3;

					var descricaoInfo3 = document.createElement("p");
					descricaoInfo3.innerText = arrayPosts[i].descricao3;

					
					containerDivTexto.appendChild(logoObra);
					containerDivTexto.appendChild(descricao);
					
					divInformacao1.appendChild(iconeInfo1);
					divInformacao1.appendChild(descricaoInfo1);

					divInformacao2.appendChild(iconeInfo2);
					divInformacao2.appendChild(descricaoInfo2);

					divInformacao3.appendChild(iconeInfo3);
					divInformacao3.appendChild(descricaoInfo3);

					divInformacoes.appendChild(divInformacao1);
					divInformacoes.appendChild(divInformacao2);
					divInformacoes.appendChild(divInformacao3);

					containerDivTexto.appendChild(divInformacoes);

					containerDivSecundaria.appendChild(containerDivTexto);
					containerDivPrincipal.appendChild(containerDivSecundaria);

					jQuery('.container-obras').append(containerDivPrincipal);
				}
				jQuery('.carregar-mais-texto').text('Mostrar mais');
				jQuery('.container-obras-pai').append( button );
				page = page + 1;
			}
		},
		error: function(err){
			jQuery('.carregar-mais-texto').text('Ocorreu algum erro');
		},
	});
}

function adicionarClasse(el, classe){
	if(el == undefined || el == null){
		return false;
	}
	if(!el.classList.contains(classe)){
		el.classList.add(classe);
	};
	return true;
}

function removerClasse(el, classe){
	if(el == undefined || el == null){
		return false;
	}		
	el.classList.remove(classe);
	return true;
}

function alterarCategoria(categoria){
	document.getElementById("categoria-produtos").value = categoria;     
	
	document.getElementById("paginacao").value = 1;
}

jQuery(document).ready(function($){
	$(".container-categoria").click(function(){
		$('#filtros_produtos').trigger('change');
	});
});


function changePage(pagina){

	document.getElementById("paginacao").value = pagina;

	jQuery(document).ready(function($){

		$('#filtros_produtos').trigger('change');

	});
}

//AJAX

//Filtros Página de Produtos
jQuery(function($){ 

	$('#filtros_produtos').change(function(){

		var filtros = $('#filtros_produtos');

		$.ajax({
			url:filtros.attr('action'),
			data:filtros.serialize(), // form data
			type:filtros.attr('method'), // POST
			beforeSend:function(xhr){
				//filtros.find('button').text('Processing...'); // changing the button label
			},
			success:function(data){
				//filtros.find('button').text('Apply filter'); // changing the button label back
				$('#produtos-encontrados').html(data); // insert data
			}
		});

		return false;

	});
});

