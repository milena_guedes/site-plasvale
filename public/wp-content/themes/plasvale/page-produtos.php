<?php
    wp_enqueue_style('css_produtos', get_stylesheet_directory_uri().'/src/css/produtos.min.css', array(), null, false);

    get_header();

    $posts_por_pagina = 16;

    empty( get_query_var('categoria') ) ? $categoriaProdutos = '' : $categoriaProdutos = get_query_var('categoria');

?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <h1><?= get_field('titulo'); ?></h1>
    <div class="container-texto">
        <?= get_field('texto'); ?>
    </div>
</div>

<div class="container-total-pai">

    <div class="container-barra-lateral">
        <aside>
            <div class="container-textos-iniciais">
                <div class="container-texto inicial">
                    <p><?= get_field('texto_onde_voce_esta'); ?></p>
                    <h1><?= get_field('texto_todos_produtos'); ?></h1>
                </div>
                <div class="container-texto">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/icone-grid-produtos.png" alt="Ícone Categorias">
                    <h1><?= get_field('titulo_categorias'); ?></h1>
                </div>
            </div>
            <div class="container-todas-categorias">
                <?php

                    $args = array(
                        'taxonomy'  => array('categoria'),
                        'post_type' => 'produtos',
                        'orderby' => 'name',
                        'parent' => '0',
                        'order'   => 'ASC',
                        'hide_empty' => false,
                    );

                    $categorias = get_categories( $args );

                    for( $i = 0; $i < count( $categorias ); $i ++ ){

                        $categoria = $categorias[$i];

                        $id = $categoria->term_id;

                        $cor_fundo = get_field('cor_de_fundo', 'categoria' . '_' . $id);
                
                ?>
                <div class="container-categoria">
                    <div class="container-selected" onclick="selecionarCategoria('<?= $categoria->slug; ?>')" style="background-color: <?= $cor_fundo; ?>">
                        <div class="cabecalho">
                            <img src="<?= get_field('icone', 'categoria' . '_' . $id); ?>" alt="Ícone da categoria <?= $categoria->name; ?>">
                            <h3 id="texto-selected"><?= $categoria->name; ?></h3>                           
                        </div>                       
                        <div class="container-seta">
                            <img id="seta-<?= $categoria->slug; ?>" src="<?php echo get_template_directory_uri()?>/img/chevron-down-solid-white.png">
                        </div>
                    </div>
                    <div class="container-options" id="container-options-<?= $categoria->slug; ?>" style="<?= $ultimo_texto; ?>">
                        <div class="container-conteudo" style="background-color: <?= $cor_fundo; ?>">
                            <?php
                            
                                $args_filho = array(
                                    'taxonomy'  => array('categoria'),
                                    'post_type' => 'produtos',
                                    'parent' => $categoria->term_id,
                                    'orderby' => 'name',
                                    'order'   => 'ASC',
                                    'hide_empty' => false,
                                );

                                $categorias_filhas = get_categories( $args_filho );

                                for( $j = 0; $j < count( $categorias_filhas ); $j ++ ){

                                    $categoria_filha = $categorias_filhas[$j];
                            ?>
                            <div class="categoria-filha" onclick="alterarCategoria('<?= $categoria_filha->slug; ?>'); "> 
                                <h1><?= $categoria_filha->name; ?></h1>
                                <div class="quantidade-produtos">
                                    <p><?= $categoria_filha->category_count; ?></p>
                                </div>
                            </div>
                            <?php
                            
                                }
                            
                            ?>
                        </div>                    
                    </div>
                </div>
                <?php
                
                    }
                
                ?>
            </div>
            <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filtros_produtos">
                <input type="hidden" name="ID-pagina" id="id-pagina">
                <input type="hidden" name="categoria-produtos" id="categoria-produtos">
                <input type="hidden" name="pagina-produtos" id="paginacao">
                <input type="hidden" name="tipo-produto" value="todos">
                <input type="hidden" name="action" value="filtrosProdutos" id="form_lancamentos"> 
            </form>
        </aside>
    </div>

    <div class="container-produtos">
        <div class="container-maior" id="produtos-encontrados">  
            <div class="container-todos-produtos">
                <?php

                    if( $categoriaProdutos ){

                        $tax_query = array(
                            array(
                                'taxonomy' => 'categoria',
                                'field' => 'slug',
                                'terms' => array( $categoriaProdutos ),
                                'hide_empty' => false,
                            ),
                        );
                
                        $argumentos = array(
                            'post_type' => 'produtos',
                            'posts_per_page' => $posts_por_pagina,
                            'post_status' => 'publish',
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'tax_query' => array( $tax_query ),
                        );

                    }else{

                        $argumentos = array(
                            'post_type' => 'produtos',
                            'posts_per_page' => 16,
                            'orderby' => 'date',
                            'order' => 'DESC'                               
                        );
                    }

                    $produtos = get_posts($argumentos);

                    for( $i = 0; $i < count( $produtos ); $i ++ ){

                        $produto = $produtos[$i];

                        $term_list = wp_get_post_terms( $produto->ID, 'categoria', array( 'orderby' => 'parent', 'order' => 'ASC' ) );

                        $id_categoria = $term_list[0]->term_id;

                        if( !empty( $id_categoria ) ){

                            $cor = get_field('cor_de_fundo', 'categoria' . '_' . $id_categoria);

                        }else{

                            $cor = '#abd6cb';
                        }

                ?>
                <div class="card-produto-pai">
                    <a href="<?= get_permalink( $produto->ID ); ?>" class="card-produto" style="border-bottom: 0px solid <?= $cor; ?>">                       
                        <img src="<?= get_field('imagens_do_produto', $produto->ID)[0]['imagem']; ?>" alt="<?= 'Imagem do produto ' . $produto->post_title; ?>">
                        <h2><?= $produto->post_title; ?></h2>
                        <p><?= get_field('ref', $produto->ID); ?></p>
                    </a>
                </div>
                
                <?php

                    }

                ?>
            </div>       
            <?php

                $argumentos = array(
                    'post_type' => 'produtos',
                    'posts_per_page' => -1,
                    'post_status' => 'publish',
                    'tax_query' => array( 'all' )         
                );

                $total_lancamentos = count( get_posts($argumentos) );

                $valor_arredondado = ceil( $total_lancamentos / $posts_por_pagina );

                $total_paginas = floor( $valor_arredondado );

                $anterior_pagina = 1;

                $proxima_pagina = 2;
            ?>

            <div class="container-pagina-pai">
                <div class="container-pagina">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/chevron-right-solid-dark.png" onclick="changePage('<?= $anterior_pagina; ?>');" class="seta seta-esquerda" alt="Seta esquerda">
                    <?php

                        $pagina = 0;
                        
                        while( $pagina < $total_paginas ){

                            $pagina += 1;

                            if( $pagina == 1 ){
                                $ativo = 'ativo';
                            }else{
                                $ativo = '';
                            }
                        
                    ?>
                    <div class="pagina <?= $ativo; ?>" onclick="changePage('<?= $pagina; ?>'); "><p><?= $pagina; ?></p></div>
                    <?php

                        }
                        
                    ?>
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/chevron-right-solid-dark.png" onclick="changePage('<?= $proxima_pagina; ?>');" class="seta seta-direita" alt="Seta direita">
                </div>
            </div>     

        </div>
    </div>
</div>

<script>

    jQuery(document).ready(function($){

        $(window).load(function() {

            selecionarCategoria('<?= $categoriaProdutos; ?>');

        });

    });

</script>


<?php get_footer();?>