<?php
    wp_enqueue_style('css_catalogos', get_stylesheet_directory_uri().'/src/css/catalogos.min.css', array(), null, false);

    get_header();
    
?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <h1><?= get_field('titulo'); ?></h1>
    <div class="container-texto">
        <?= get_field('texto'); ?>
    </div>
</div>

<div class="container-banner-catalogos">
    <?php

        $banners_catalogo = get_field('banners_de_catalogos'); 
    
        for( $i = 0; $i < count( $banners_catalogo ); $i ++ ){

            $banner = $banners_catalogo[$i];
    
    ?>
    <div class="container-banner" style="background-image: url('<?= $banner['imagem_de_fundo']; ?>'); ">
        <div class="container-pequeno">
            <div class="container-inicial">
                <h1><?= $banner['titulo']; ?></h1>
                <div class="descricao">
                    <?= $banner['descricao']; ?>
                </div>            
            </div>
            <div class="carrossel-catalogos-pai">

                <?php
                
                    if( count( $banner['catalogos_do_carrossel'] ) <= 1 ){

                        $seta = 'disable';
                        
                    }else{
                        $seta = '';
                    }
                
                ?>
        
                <div class="seta <?= $seta; ?> seta-anterior-<?= $i + 1; ?>"> 
                    <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-left-solid.png">  
                </div>
            
                <div class="carrossel-catalogos-filho carrossel-catalogos-filho-<?= $i + 1; ?>">
                    
                   <?php

                        $catalogos = $banner['catalogos_do_carrossel']; 
                            
                        for( $j = 0; $j < count( $catalogos ); $j ++ ){

                            $catalogo = $catalogos[$j]['catalogo'];

                    ?>  
                    <div class="card-catalogo">
                        <img src="<?= get_field('imagem', $catalogo->ID); ?>" alt="Imagem do Catalogo <?= $catalogo->post_tile; ?>">
                        <div class="conteudo">
                            <h1><?= $catalogo->post_title; ?></h1>
                            <p class="ano"><?= get_field('ano', $catalogo->ID); ?></p>

                            <a href="<?= get_field('arquivo', $catalogo->ID)['url']; ?>" target="_blank" class="botao-ver-online">
                                <p><?= $banner['texto_ver_online']; ?></p>
                            </a>
            
                            <a href="<?= get_field('arquivo', $catalogo->ID)['url']; ?>" download="<?= get_field('arquivo', $catalogo->ID)['filename']; ?>" class="botao-baixar">
                                <p><?= $banner['texto_baixar']; ?></p>
                            </a>                            
                        </div>
                    </div>
                    <?php

                        }
                    
                    ?>
                
                </div>

                
                <div class="seta <?= $seta; ?> seta-proxima-<?= $i + 1; ?>">
                    <img src="<?=get_stylesheet_directory_uri()?>/img/chevron-right-solid.png">  
                </div>

            
            </div>

        </div>
        
    </div>
    <?php

        }
    
    ?>
</div>

<script>

    jQuery(document).ready(function ($) {

        $('.carrossel-catalogos-filho-1').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: false,
            prevArrow: $('.seta-anterior-1'),
            nextArrow: $('.seta-proxima-1'),
            responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			],
        });

        $('.carrossel-catalogos-filho-2').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: false,
            prevArrow: $('.seta-anterior-2'),
            nextArrow: $('.seta-proxima-2'),
            responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			],
        });

        $('.carrossel-catalogos-filho-3').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: false,
            prevArrow: $('.seta-anterior-3'),
            nextArrow: $('.seta-proxima-3'),
            responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			],
        });

        $('.carrossel-catalogos-filho-4').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: false,
            prevArrow: $('.seta-anterior-4'),
            nextArrow: $('.seta-proxima-4'),
            responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			],
        });

        $('.carrossel-catalogos-filho-5').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            fade: false,
            prevArrow: $('.seta-anterior-5'),
            nextArrow: $('.seta-proxima-5'),
            responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
			],
        });

    });

</script>


<?php
    get_footer();
?>