<?php

    add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
    
    function create_topics_hierarchical_taxonomy() {
    
        $labels = array(
            'name' => _x( 'Categorias', 'taxonomy general name' ),
            'singular_name' => _x( 'Categoria', 'taxonomy singular name' ),
            'search_items' =>  __( 'Buscar Categorias' ),
            'all_items' => __( 'Todos as Categorias' ),
            'parent_item' => __( 'Categoria Pai' ),
            'parent_item_colon' => __( 'Categoria Pai:' ),
            'edit_item' => __( 'Editar Categoria' ), 
            'update_item' => __( 'Atualizar Categoria' ),
            'add_new_item' => __( 'Adicionar Nova Categoria' ),
            'new_item_name' => __( 'Adicionar Nova Categoria' ),
            'menu_name' => __( 'Categorias' ),
        );    
        
        // Now register the taxonomy
        
        register_taxonomy(
            'categoria',
            array( 
                'produtos',
            ),
            array(
                'rewrite' => false,
                'hierarchical' => true,
                'labels' => $labels,
                'show_ui' => true,
                'show_admin_column' => true,
                'query_var' => true
            )
        );

    }

