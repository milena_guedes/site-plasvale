<?php

    //Filtros produtos
    function filtros_produtos(){

        $posts_por_pagina = 16;

        $queryFiltros = array();

        $ID_pagina = $_POST['ID-pagina'];

        $categoria = $_POST['categoria-produtos'];

        $tipo = $_POST['tipo-produto'];

        $page = $_POST['pagina-produtos'];

        if( empty( $page ) ){
            $page = 1;
        }

        $proxima_pagina = $page + 1;

        if( $page == 1 ){
                                    
        $anterior_pagina = 1;

        }else{

            $anterior_pagina = $page - 1;
        }

        $meta_query = array(
            array(
                'key' => 'esse_produto_e_um_lancamento',
                'value' => true,
                'compare' => '='
            ),
        );

        if( !empty( $categoria ) ){

            $tax_query = array(
                array(
                    'taxonomy' => 'categoria',
                    'field' => 'slug',
                    'terms' => array( $categoria ),
                    'hide_empty' => false,
                ),
            );
    
            $argumentos = array(
                'post_type' => 'produtos',
                'posts_per_page' => $posts_por_pagina,
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'tax_query' => array( $tax_query ),
                'meta_query'=> array( $meta_query ),
                'paged' => $page
            );

        }else{

            $argumentos = array(
                'post_type' => 'produtos',
                'posts_per_page' => $posts_por_pagina,
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'tax_query' => array( 'all' ),
                'meta_query'=> array( $meta_query ),
                'paged' => $page
            );
        }

        if( $tipo == 'todos' ){

            $argumentos = array(
                'post_type' => 'produtos',
                'posts_per_page' => $posts_por_pagina,
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'tax_query' => array( $tax_query ),
                'paged' => $page
            );

            if( empty( $categoria ) ){

                $argumentos = array(
                    'post_type' => 'produtos',
                    'posts_per_page' => $posts_por_pagina,
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'tax_query' => array( 'all' ),
                    'paged' => $page
                );
            }
        }

        $produtos = get_posts($argumentos);

        $contadorProdutos = count($produtos);

        if( $produtos ){ ?>

            <div class="container-todos-produtos">

                <?php

                    for( $p = 0;  $p < $contadorProdutos; $p ++){

                        $produto = $produtos[$p];

                        $term_list = wp_get_post_terms( $produto->ID, 'categoria', array( 'orderby' => 'parent', 'order' => 'ASC' ) );

                        $id_categoria = $term_list[0]->term_id;

                        if( !empty( $id_categoria ) ){

                            $cor = get_field('cor_de_fundo', 'categoria' . '_' . $id_categoria);

                        }else{

                            $cor = '#abd6cb';
                        }
                ?>

                            
                <div class="card-produto-pai">
                    <a href="<?= get_permalink( $produto->ID ); ?>" class="card-produto" style="border-bottom: 0px solid <?= $cor; ?>">                       
                        <img src="<?= get_field('imagens_do_produto', $produto->ID)[0]['imagem']; ?>" alt="<?= 'Imagem do produto ' . $produto->post_title; ?>">
                        <h2><?= $produto->post_title; ?></h2>
                        <p><?= get_field('ref', $produto->ID); ?></p>
                    </a>
                </div>          
                            

                <?php } ?>

            </div>

        <?php

            if( !empty( $categoria ) ){

                $total_lancamentos = contadorLancamentos($categoria);

            }else{
                $argumentos = array(
                    'post_type' => 'produtos',
                    'posts_per_page' => -1,
                    'post_status' => 'publish',
                    'tax_query' => array( 'all' ),
                    'meta_query'=> array( $meta_query )           
                );

                $total_lancamentos = count( get_posts($argumentos) );
            }

            $valor_arredondado = ceil( $total_lancamentos / $posts_por_pagina );

            $total_paginas = floor( $valor_arredondado );

            if( $total_paginas > 1 ){
        ?>

            <div class="container-pagina-pai">
                <div class="container-pagina">
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/chevron-right-solid-dark.png" onclick="changePage('<?= $anterior_pagina; ?>');" class="seta seta-esquerda" alt="Seta esquerda">
                    <?php

                        $pagina = 0;
                    
                        while( $pagina < $total_paginas ){

                            $pagina += 1;

                            if( $page == $pagina ){

                                $ativo = 'ativo';

                            }else{
                                $ativo = '';
                            }
                    
                    ?>
                    <div class="pagina <?= $ativo; ?>" onclick="changePage('<?= $pagina; ?>'); "><p><?= $pagina; ?></p></div>
                    <?php

                        }
                    
                    ?>
                    <img src="<?= get_stylesheet_directory_uri() ?>/img/chevron-right-solid-dark.png" onclick="changePage('<?= $proxima_pagina; ?>');" class="seta seta-direita" alt="Seta direita">
                </div>
            </div>
            <?php
            }

        }

        die();
    }

add_action('wp_ajax_filtrosProdutos', 'filtros_produtos'); 
add_action('wp_ajax_nopriv_filtrosProdutos', 'filtros_produtos');