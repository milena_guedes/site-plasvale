<?php
add_action( 'init', function(){

    add_rewrite_tag('%categoria%', '([0-9a-zA-Z\-]*)');

    add_rewrite_tag('%item-pesquisado%', '([0-9a-zA-Z\-]*)');

    add_rewrite_rule('^produtos/categoria/([0-9a-zA-Z\-]*)?','index.php?pagename=produtos&categoria=$matches[1]', 'top');

    add_rewrite_rule('^produtos-en/categoria/([0-9a-zA-Z\-]*)?','index.php?pagename=produtos-en&categoria=$matches[1]', 'top');

    add_rewrite_rule('^produtos-es/categoria/([0-9a-zA-Z\-]*)?','index.php?pagename=produtos-es&categoria=$matches[1]', 'top');


    add_rewrite_rule('^pesquisa/([0-9a-zA-Z\-]*)?','index.php?pagename=pesquisa&item-pesquisado=$matches[1]', 'top');

    add_rewrite_rule('^pesquisa-en/([0-9a-zA-Z\-]*)?','index.php?pagename=pesquisa-en&item-pesquisado=$matches[1]', 'top');

    add_rewrite_rule('^pesquisa-es/([0-9a-zA-Z\-]*)?','index.php?pagename=pesquisa-es&item-pesquisado=$matches[1]', 'top');


    flush_rewrite_rules();

});
