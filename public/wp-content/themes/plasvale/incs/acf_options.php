<?php
// APENAS SE O PLUGIN ACF OPTIONS ESTIVER ATIVO
// Adiciona submenus na aba options do plugin ACF
if (function_exists('acf_add_options_sub_page')) {
    
    $slugs_paginas = array('geral', 'header', 'footer');
    foreach ( $slugs_paginas as $lang ) {
        acf_add_options_sub_page( array(
        'page_title' => ucfirst( $lang ),
        'menu_title' => __(ucfirst( $lang ), 'text-domain'),
        'menu_slug'  => $lang,
        'post_id'    => $lang,
        ) );
    }
}