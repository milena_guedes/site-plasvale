<?php
    wp_enqueue_style('css_onde_comprar', get_stylesheet_directory_uri().'/src/css/onde-comprar.min.css', array(), null, false);

    get_header();

?>

<div class="container-banner-inicial" style="background-image: url('<?= get_field('imagem_de_fundo'); ?>'); ">
    <h1><?= get_field('titulo'); ?></h1>
    <div class="container-texto">
        <?= get_field('texto'); ?>
    </div>
</div>

<div class="container-informacoes-contato">
    <div class="container-medio">
        <div class="container-formulario">
            <form data-hc-form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
                <div class="linha">
                    <input type="hidden" name="onde encontrar" value="Formulário Onde encontrar" hc-mail-message>
                    <input type="text" name="nome" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['input_nome']; ?>">
                    <div class="vazia"></div>
                    <input type="text" name="email" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['input_email']; ?>">
                </div>
                <div class="linha">
                    <input type="text" class="telefone" name="telefone" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['input_telefone']; ?>">
                </div>
                <div class="linha">
                    <input type="text" name="cidade" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['input_cidade']; ?>">
                    <div class="vazia"></div>
                    <input type="text" name="UF" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['input_uf']; ?>">
                </div>
                
                <textarea name="mensagem" required hc-mail-message placeholder="<?= get_field('formulario_de_contato')['input_mensagem']; ?>"></textarea>
                <div class="container-botoes">
                    <button type="submit" class="botao"><?= get_field('formulario_de_contato')['texto_botao']; ?></button>
                </div>
                
                <span data-hc-feedback></span>
            </form>
        </div>
    </div>
    <div class="container-acessar-loja">
        <a href="<?= get_field('formulario_de_contato')['link_para_loja_virtual']; ?>" class="botao"><?= get_field('formulario_de_contato')['texto_botao_acessar_loja']; ?></a>
    </div>
</div>

<script>

    jQuery(document).ready(function($){

        $('.telefone').mask('(00) 00000-0000');

    });

</script>


<?php get_footer(); ?>